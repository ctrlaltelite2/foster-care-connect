-   Jan 16, 2024: We set up the tables for our postgres database.
-   Jan 17, 2024: We did all the authentication stuff as a group while rewatching Riley's lecture.
-   Jan 18, 2024: We set up the rest of the queries and routers. I did the Toys queries and router. For some reason the get functions aren't functioning.
-   Jan 19, 2024: I found out the reason for the get functions not working. Turns out the record in the queries had the table columns out of order.

    """
    def record_to_toys_out(self, record):
    return ToyOut(
    id=record[0],
    name=record[1],
    picture=record[2],
    description=record[3],
    condition=record[4],
    children_id=record[5]
    )
    """

    these have to be in the same order as the miagration table
    Backend Finished

-   Jan 22, 2024: We set up the file structure and started woring on out front-end auth. We took a lot of the day for expolorations becuase we are ahead and want to be sure we know the material before diving in.
-   Jan 23, 2024: we continued to work on auth and login works. DOCKER ruins everything but we got through it
-   Jan 24, 2024: We built and designed the navBar, and homePage and we built out most of our sliced. we got the redirect from the parents login to the parent profile to work.
-   Jan 25, 2024: had to change some of our error handling so our token can become null when we logout. the parent and agent login and basic profiles created.
-   Jan 26, 2024: finished parent profile, and edit parent profile.
-   Jan 27, 2024: Figured out out render error was do to some of the slices being wrong and being imported into the child profile, finished the MVP of the front-end.
-   Jan 29, 2024: finished child profile, and edit lists pages
-   Jan 30, 2024: Finished MVP started Delpoying
-   Jan 31, 2024: cont.. Delpoying Ran out of Minutes...
-   Feb 1, 2024: Minutes refreshed on the 1st. successfuly deployed.
    Deployment took a lot of work but Now I will ensure to delete unneeded imports. but MVP is COMPLETE and Fully deployed
-   Feb 2, 2024: Worked on stretch goals with the team
-   Feb 5, 2024: helped my team work on stetch goals I feel like all this is very easy for me. I feel im very good at problem solving. It really hard for me to not to just do all the stretch goals... I trying to make sure everyone has a oppurtunity to try/ do whatever they want and be here to help them if needed.
-   Feb 6, 2024: Added delete accounts to both the agent and parent profile so it will now delete both the account and the parent/agent at the same time
-   Feb 7, 2024: added the feature so the agent can switch between all the children and all the parents the is easy as i have done it before in mod2.
