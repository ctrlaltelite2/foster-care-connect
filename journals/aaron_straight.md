01/16/24:
    Set up the database in it's simpliest form. We still need to set up the FK references but Anna will take the 
    lead on that since she has the most experienced with postgres. Tomorrow we plan to work on setting up the 
    authenticator so that we can begin routers/queries.
01/17/24
    We got the authenticator into our project and tested! This was a very coonfusing time just with
    having to switch the lecture to postgres. The docs were extremly helpful in this however.
    Tomorrow we plan to build the fastapi routers and queries for our tables!
01/18/24
    Got the fastAPI built! Tok awhile but we now have all the CRUD routes written out. However, now we are
    in the middle of error testing. Right now we are getting type-errors and dict errors. The data 
    is also in some cases not being displyed as a list when it needs to be. Tomorrow we will be back
    in swagger until all the crud routes work.
01/19/24
    Finish error testing for the backend! everything works, but the pages are all slightly different.
    It might be a good idea to go back in to make them uniform. Weekend earned.
01/22/24
    Really poor performance today. We set aside the day to reasearch front end and I pissed it away. Very upset
    with myself but need to take it as a learning moment. We may be close to the end of the course, but that makes 
    finishing strong all the more important!! Spoke with Kaylee about accountability. Tomorrow I plan on working 
    with group to implement the front-end auth and then start building out the react pages. 
1/23/24
    Much better today with work ethic. We started adding the front in auth, built out the agent login, parent login, and a regular login to work in the front-end. We also started the forms for those as well. When we got to a good point with those we wanted to test them but ran into a major issue with our docker not working. The error would kick us out of the ghi container and would allow us to run it. To fix this we had to delete the node_modules and the package-lock.json then rebuild. This fixed the issue! Good to know for the future!! Tomorrow we plan on testing what we built, then moving forward with building out the rest of the front-end framework.
1/24/24
    Worked on the front end authorization and all our forms. We still need to test all the forms for functionality but feeling confident that they work. Using the docs along with writing the code was king today, they are a bit hard to read but it really helped. The docs arent written in a way where you can just look up a word like a dictionary, it is more so a walkthrough of someone writing code and you have to reverse engineer it. Tomorrow I think we are just going to push forward in building out the front end! Need to do the list pages and the profile pages.
1/26/24
    We are currently working to finish up the front end. We have started the profiles and styling the forms, I am focusing in on the agent's profile. I am trying to get the page to display the children as cards with the ability to filter by parents. I am struggling to get all the cards to display by default, right now you have to select a parent to see results. On monday I will be looking to finish that page up.
1/30/24
    Today we spent our time deep in error testing/fixing. We tackled the pipeline error that was causing our gitlab pipeline to come back as broken. We installed black and also had to go through each page and remove variables defined but never called, apostrophies, and various small formatting errors. We were able to fix the pipeline and started the deployment process. We ended with the site deployed but the api is not working so tomorrow we will be focusing on testing/fixing those errors.
1/31/24
    Spent today getting the final steps for deployment, however, we are completly out of minutes. We cannot do anything until we  get those minutes so right now we are just waiting. Tomorrow, pending minutes, we will test our deployment and fix any errors that come up.
2/1/24
    MVP is complete! We are now fully deployed and running! The deployment was more difficult than any of us thought and we had to do a lot of small things to make it work. It was surprising how much came down to code cleanup and organization. Next week we will be fully focusing on stretch goals, I am going to start by adding the filter functionality to the agent's profile page.
2/5/24
    Was able to figure out the stretch goal I had defined for myself. I wanted the agent profile to be able to filter the child cards by their specific parents. The thing that made this difficult for me was getting the cards to display by default when no parent is selected in the dropdown. In order to do this I had to give the "Select Parent.." option a value of an empty string, then give it the function to show all if an empty string is selected. Tomorrow I want to get started on the ReadMe file.
2/6/24
    Wrote the entire readme file yesterday. I made sure to put extra detail and lots of images, pretty happy with the result. After that I just worked on knocking out issues and adding stretch goals. It feels good to be so far ahead because now we can just take our time adding features. It is honestly a good lesson for the future, I am notorious for doing things last minute whereas I could be taking the time to make things clean and polished. Tomorrow I plan on implementing an about/contact page as well as any issues that I can knockout from the gitlab.
2/7/24
    Today was not a very productive day as far as quantity of content. I did fix some minor errors and cleaned  up some pages, but as far as putting in new features I did not add anything to the site. While that is not a bad thing seeing as we have our project done and deployed, I still want to stay on top of things and constantly be pushing myself to improve. Tommorrow I will do our about page and make sure it looks good!
2/8/24
    About page went really well! I am actually very happy with the result and very proud of the things I implemented into the page. I really am starting to feel a real confidence with my code and really looking forward to doing this professionally. Tommorrow will be the final testing and buttoning down all the hatches.
