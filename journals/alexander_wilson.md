01/17/24:
Successfully integrated the authenticator into our project and conducted testing. The transition to using Postgres was challenging but was made smoother thanks to the comprehensive documentation. Our next step is to develop the FastAPI routers and queries for our database tables.

01/18/24:
We've completed the construction of our FastAPI, including all CRUD routes. Currently, we're troubleshooting some type and dictionary errors, and addressing issues with data not displaying correctly in list format. We aim to resolve these issues in Swagger tomorrow.

01/19/24:
Completed error testing for the backend. All functions are operational, but there's a lack of consistency across pages. We're considering revisiting this to standardize the interface. After a productive week, we're ready for the weekend.

01/22/24:
Today was less productive than expected, which was disappointing. However, it's a valuable lesson in staying focused, especially as the course nears its end. Tomorrow, we plan to collaborate on implementing front-end authentication and start developing the React pages.

1/23/24:
Productivity was back on track today. We initiated front-end authentication and developed several login forms, including agent, parent, and regular user logins. Faced a significant setback with Docker which was resolved by deleting node_modules and package-lock.json, then rebuilding. This fix is worth remembering for future reference. Our next steps involve testing the new features and continuing with front-end development.

1/24/24:
Focused on finalizing front-end authorization and form functionality. Using documentation alongside coding was challenging but ultimately helpful. The format of the docs requires a bit of reverse engineering to fully understand. Moving forward, our goal is to develop list pages and profile pages for the front end.


1/25/24:
I have gained valuable experience by developing the profile page for children and the list for toys. It was enlightening to observe how various components interact with each other within the React-Redux ecosystem.


1/26/24:
Working on finishing our front end I cleaned up some of our code and added css to our agent login page aswell as updated some code inside the agent login form.

1/30/24
 our focus was on identifying and resolving errors, particularly addressing a malfunction within our GitLab pipeline that was marking it as defective. We integrated Black for code formatting and conducted a thorough review of our codebase. This involved eliminating unused variables, correcting improper use of apostrophes, and making various minor formatting adjustments. These efforts led to the successful rectification of the pipeline issue, enabling us to initiate the deployment phase. Although the deployment of the site was completed, we encountered functionality problems with the API. Consequently, our next steps will involve concentrating on diagnosing and fixing the API-related issues.


1/31/24
We have been working on deploying our app but have currently ran out of minutes so we are waiting to recieve those so that we can continue to progress with the deployment of our application.

2/1/24
Minimum Viable Product (MVP) has successfully reached completion and is now fully operational! The deployment process proved to be more challenging than anticipated, requiring significant effort in refining and organizing our code. It's remarkable how crucial these seemingly minor adjustments were to our success. Moving forward, our attention will shift towards achieving our stretch goals, starting with the implementation of a filter feature on the agent's profile page next week.


2/6/24
I introduced a new feature that enables the retrieval of accounts using their unique IDs. This enhancement allows for more precise and efficient access to account information. Users can now easily query specific accounts directly by their identifiers.
