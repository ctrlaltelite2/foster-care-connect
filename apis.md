# APIs

## Agents

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/agents```, ```/api/agents/{agent_id}```

Input:
```
{
    "name": "string",
    "employee_id": 0,
    "account_id": 0,
    "profile_picture": "string"
}
```
Output:
```
{
    "id": 0,
    "name": "string",
    "employee_id": 0,
    "account_id": 0,
    "profile_picture": "string"
}
```
Creating a new agent also creates their profile and gives them access to add children and parents as well.

---

## Parents

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/parents```, ```/api/parents/{parent_id}```

Input:
```
{
    "parent1_name": "string",
    "parent2_name": "string",
    "location": "string",
    "account_id": 0,
    "profile_picture": "string"
}
```
Output:
```
{
    "id": 0,
    "parent1_name": "string",
    "parent2_name": "string",
    "location": "string",
    "account_id": 0,
    "profile_picture": "string"
}
```
Creating a new parent gives them access to their profile and children assigned to them by an agent. From their profile they can add Incidents, Medical Appointments, Medications, and Toys to their assigned children

---

## Children

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/children```, ```/api/children/{child_id}```

Input:
```
{
    "profile_picture": "string",
    "name": "string",
    "age": 0,
    "parents_id": 0,
    "agents_id": 0
}
```
Output:
```
{
    "id": 0,
    "profile_picture": "string",
    "name": "string",
    "age": 0,
    "parents_id": 0,
    "agents_id": 0
}
```
Creating a new child creates their profile page that lists all assigned Incidents, Medical Appointments, Medications, and Toys

---

## Incidents

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/incidents```, ```/api/incidents/{incident_id}```

Input:
```
{
    "name": "string",
    "picture": "string",
    "description": "string",
    "police_involved": true,
    "action_plan": "string",
    "children_id": 0
}
```
Output:
```
{
    "id": 0,
    "name": "string",
    "picture": "string",
    "description": "string",
    "police_involved": true,
    "action_plan": "string",
    "children_id": 0
}
```
Tied to a specific child by the children_id and displays on that child's profile page

---

## Toys

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/toys```, ```/api/toys/{toy_id}```

Input:
```
{
    "name": "string",
    "picture": "string",
    "description": "string",
    "condition": "string",
    "children_id": 0
}
```
Output:
```
{
    "id": 0,
    "name": "string",
    "picture": "string",
    "description": "string",
    "condition": "string",
    "children_id": 0
}
```
Tied to a specific child by the children_id and displays on that child's profile page

---

## Medications

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/medications```, ```/api/medications/{medication_id}```

Input:
```
{
    "name": "string",
    "dosage": "string",
    "quantity": 0,
    "expiration_date": "2024-02-06",
    "children_id": 0
}
```
Output:
```
{
    "id": 0,
    "name": "string",
    "dosage": "string",
    "quantity": 0,
    "expiration_date": "2024-02-06",
    "children_id": 0
}
```
Tied to a specific child by the children_id and displays on that child's profile page

---

## Medical Appointments

- Method: ```GET```, ```POST```, ```PUT```, ```DELETE```
- Path: ```/api/med_appts```, ```/api/med_appts/{med_appt_id}```

Input: 
```
{
    "date": "2024-02-06",
    "location": "string",
    "reason": "string",
    "description_of_care": "string",
    "treatment_plan": "string",
    "children_id": 0
}
```
Output:
```
{
    "id": 0,
    "date": "2024-02-06",
    "location": "string",
    "reason": "string",
    "description_of_care": "string",
    "treatment_plan": "string",
    "children_id": 0
}
```
Tied to a specific child by the children_id and displays on that child's profile page

---

## Authenticator

- Method: ```GET```, ```POST```, ```POST```, ```PUT```, ```DELETE```, ```DELETE```
- Path: ```/token```, ```/api/account```, ```/api/accout/{id}```

Input for Token:
```
{
    "username": "string"
    "password": "string"
}
```
Ouput for Token:
```
{
    "access_token": "string",
    "token_type": "Bearer"
}
```
Input for Account:
```
{
    "username": "string"
    "password": "string"
}
```
Output for Account:
```
{
    "access_token": "string",
    "token_type": "Bearer",
    "account": {
        "id": 0,
        "username": "string"
  }
}
```
The authenticator in added in with the creation of an Agent or Parent form on our frontend, allowing the creating agent to assign usernames and passwords during that creation process
