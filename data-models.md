# Data Models

## Agents

| name            | type                  | unique | optional |
|-----------------|-----------------------|--------|----------|
| id              | int                   | yes    | no       |
| profile_picture | string                | no     | yes      |
| name            | string                | no     | no       |
| employee_id     | int                   | no     | no       |
| account_id      | Reference to Accounts | yes    | no       |

---

## Parents

| name            | type                  | unique | optional |
|-----------------|-----------------------|--------|----------|
| id              | int                   | yes    | no       |
| profile_picture | string                | no     | yes      |
| parent1_name    | string                | no     | no       |
| parent2_name    | string                | no     | no       |
| location        | string                | no     | no       |
| account_id      | Reference to Accounts | yes    | no       |

---

## Children

| name            | type                  | unique | optional |
|-----------------|-----------------------|--------|----------|
| id              | int                   | yes    | no       |
| profile_picture | string                | no     | yes      |
| name            | string                | no     | no       |
| age             | int                   | no     | no       |
| parents_id      | Reference to Parents  | yes    | no       |
| agents_id       | Reference to Agents   | yes    | no       |

---

## Medications

| name            | type                  | unique | optional |
|-----------------|-----------------------|--------|----------|
| id              | int                   | yes    | no       |
| name            | string                | no     | no       |
| dosage          | string                | no     | no       |
| quantity        | int                   | no     | no       |
| expiration_date | date                  | no     | no       |
| children_id     | Reference to Children | yes    | no       |

---

## Incidents

| name            | type                  | unique | optional |
|-----------------|-----------------------|--------|----------|
| id              | int                   | yes    | no       |
| name            | string                | no     | no       |
| picture         | string                | no     | yes      |
| description     | string                | no     | no       |
| police_involved | bool                  | no     | no       |
| action_plan     | string                | no     | no       |
| children_id     | Reference to Children | yes    | no       |

---

## Medical Appointments

| name                | type                  | unique | optional |
|---------------------|-----------------------|--------|----------|
| id                  | int                   | yes    | no       |
| date                | date                  | no     | no       |
| location            | string                | no     | no       |
| reason              | string                | no     | no       |
| description_of_care | bool                  | no     | no       |
| treatment_plan      | string                | no     | no       |
| children_id         | Reference to Children | yes    | no       |

---

## Toys

| name        | type                  | unique | optional |
|-------------|-----------------------|--------|----------|
| id          | int                   | yes    | no       |
| name        | string                | no     | no       |
| picture     | string                | no     | yes      |
| description | string                | no     | no       |
| condition   | string                | no     | no       |
| children_id | Reference to Children | yes    | no       |

---

## Accounts

| name            | type   | unique | optional |
|-----------------|--------|--------|----------|
| id              | int    | yes    | no       |
| username        | string | yes    | no       |
| hashed_password | string | yes    | no       |

---
