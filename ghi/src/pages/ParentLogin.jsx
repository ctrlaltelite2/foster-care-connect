import { useState, useEffect } from 'react'
import { useLoginMutation } from '../features/auth/authSlice'
import { useGetAllParentsQuery } from '../features/Slices/ParentSlice'
import { useNavigate } from 'react-router-dom'
import { useGetTokenQuery } from '../features/auth/authSlice'
import '../css/ParentLogin.css'

const ParentLogin = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const [login] = useLoginMutation()
    const navigate = useNavigate()
    const { data: tokenData } = useGetTokenQuery()
    const { data: parentsData } = useGetAllParentsQuery()
    useEffect(() => {
        if (tokenData && parentsData) {
            const matchingParent = parentsData.find(
                (parent) => parent.account_id === tokenData.account.id
            )

            if (matchingParent) {
                navigate(`/parent/profile/${matchingParent.id}`)
            }
        }
    }, [tokenData, parentsData, navigate])

    const handleLogin = async (e) => {
        e.preventDefault()
        login({
            username,
            password,
        })
    }

    return (
        <div className="parentLogin">
            <h2 className="parentLoginTitle">Welcome Parents</h2>
            <form className="parentLoginForm" onSubmit={handleLogin}>
                <div className="parentLoginFormInput">
                    <label htmlFor="username">Username</label>
                    <input
                        type="text"
                        id="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        required
                    />
                </div>
                <div className="parentLoginFormInput">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </div>
                <button className="parentLoginFormButton" type="submit">
                    Login
                </button>
            </form>
        </div>
    )
}

export default ParentLogin
