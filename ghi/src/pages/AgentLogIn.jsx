import { useState, useEffect } from 'react'
import { useLoginMutation } from '../features/auth/authSlice'
import { useGetAllAgentsQuery } from '../features/Slices/AgentSlice'
import { useNavigate } from 'react-router-dom'
import { useGetTokenQuery } from '../features/auth/authSlice'
import '../css/AgentLogin.css'

const AgentLogin = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const [login] = useLoginMutation()
    const navigate = useNavigate()
    const { data: tokenData } = useGetTokenQuery()
    const { data: agentsData } = useGetAllAgentsQuery()

    useEffect(() => {
        if (tokenData && agentsData) {
            const matchingAgent = agentsData.find(
                (agent) => agent.account_id === tokenData.account.id
            )

            if (matchingAgent) {
                navigate(`/agent/profile/${matchingAgent.id}`)
            }
        }
    }, [tokenData, agentsData, navigate])

    const handleLogin = async (e) => {
        e.preventDefault()
        login({
            username,
            password,
        })
    }

    return (
        <div className="agentLogin">
            <h2 className="agentLoginTitle">Welcome Agents</h2>
            <form className="agentLoginForm" onSubmit={handleLogin}>
                <div className="agentLoginFormInput">
                    <label htmlFor="username">Username</label>
                    <input
                        type="text"
                        id="username"
                        className="agentLoginInput"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        required
                    />
                </div>
                <div className="agentLoginFormInput">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        className="agentLoginInput"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </div>
                <button className="agentLoginFormButton" type="submit">
                    Login
                </button>
            </form>
        </div>
    )
}

export default AgentLogin
