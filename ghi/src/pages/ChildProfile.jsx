import { useParams, Link, useNavigate } from 'react-router-dom'
import { skipToken } from '@reduxjs/toolkit/query/react'
import { useGetChildByIdQuery } from '../features/Slices/ChildSlice'
import { useGetIncidentsQuery } from '../features/Slices/IncidentSlice'
import { useGetMedApptsQuery } from '../features/Slices/medApptSlice'
import { useGetAllToysQuery } from '../features/Slices/ToySlice'
import { useGetAllMedicationsQuery } from '../features/Slices/MedicationSlice'
import { useGetParentByIdQuery } from '../features/Slices/ParentSlice'
import { useGetAgentByIdQuery } from '../features/Slices/AgentSlice'
import ToysList from '../components/ChildLists/ToysList'
import MedicationList from '../components/ChildLists/MedicationList'
import MedApptsList from '../components/ChildLists/MedicalApptList'
import IncidentsList from '../components/ChildLists/IncidentsList'
import CustomCalendar from '../components/CustomCalendar'
import defaultProfile from '../assets/defaultProfile.jpg'
import '../css/ChildProfile.css'

const ChildProfile = () => {
    const { id } = useParams()
    const childId = parseInt(id)
    const navigate = useNavigate()
    const {
        data: child,
        isLoading: childisLoading,
        error,
    } = useGetChildByIdQuery(childId)

    const { data: parent, isLoading: parentisLoading } = useGetParentByIdQuery(
        child?.parents_id ?? skipToken
    )
    const { data: agent, isLoading: agentisLoading } = useGetAgentByIdQuery(
        child?.agents_id ?? skipToken
    )

    const { data: incidents, isLoading: incidentLoading } =
        useGetIncidentsQuery()

    const { data: medappts, isLoading: medapptLoading } = useGetMedApptsQuery()

    const { data: toys, isLoading: toyLoading } = useGetAllToysQuery()

    const { data: medications, isLoading: medicationLoading } =
        useGetAllMedicationsQuery()

    const handleUpdate = () => {
        navigate(`/child/editprofile/${id}`)
    }

    if (
        childisLoading ||
        incidentLoading ||
        parentisLoading ||
        agentisLoading ||
        medapptLoading ||
        toyLoading ||
        medicationLoading
    )
        return <div>Loading child profile...</div>
    if (error) return <div>Error: {error.message}</div>

    return (
        <div>
            <h1 className="childProfileWelcome">
                Little {child.name}&apos;s Dashboard{' '}
            </h1>
            <div className="childProfileInfo">
                <div className="childProfileImgInfo">
                    <div className="childProfileImgUpdate">
                        <img
                            src={
                                child.profile_picture !== ''
                                    ? child.profile_picture
                                    : defaultProfile
                            }
                            alt={`${child.name}`}
                            className="childProfilePicture"
                        />

                        <button
                            className="childProfileUpdateBtn"
                            onClick={handleUpdate}
                        >
                            Update Profile
                        </button>
                    </div>
                    <div className="childProfileDetails">
                        <p>Name: {child.name}</p>
                        <p>Age: {child.age}</p>
                        <p>Parent 1: {parent.parent1_name}</p>
                        <p>Parent 2: {parent.parent2_name}</p>
                        <p>Agent: {agent.name}</p>
                        <p>Agents Employee ID: {agent.employee_id}</p>
                    </div>
                </div>
                <div className="childProfileLinks">
                    <Link
                        className=""
                        to={`/child/profile/${childId}/create-incident`}
                    >
                        <p>Add an Incident</p>
                    </Link>
                    <Link
                        className=""
                        to={`/child/profile/${childId}/create-medappt`}
                    >
                        <p>Add a Medical Appt.</p>
                    </Link>
                    <Link
                        className=""
                        to={`/child/profile/${childId}/create-medication`}
                    >
                        <p>Add a Medication</p>
                    </Link>
                    <Link
                        className=""
                        to={`/child/profile/${childId}/create-toy`}
                    >
                        <p>Add a Toy</p>
                    </Link>
                </div>
            </div>

            <div className="childLists">
                <div className="calendarCard">
                    <CustomCalendar childId={id} appointments={medappts} />
                </div>
                <div className="childList">
                    <IncidentsList childId={id} incident={incidents} />
                </div>
                <div className="childList">
                    <MedApptsList childId={childId} medappt={medappts} />
                </div>
                <div className="childList">
                    <MedicationList
                        childId={childId}
                        medication={medications}
                    />
                </div>
                <div className="childList">
                    <ToysList childId={childId} toy={toys} />
                </div>
            </div>
        </div>
    )
}

export default ChildProfile
