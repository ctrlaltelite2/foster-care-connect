import  { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetMedApptByIdQuery,
    useUpdateMedApptsMutation,
} from '../features/Slices/medApptSlice'
import '../css/MedicalApptForm.css'

const MedApptEditForm = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const { data: appointment, isLoading } = useGetMedApptByIdQuery(id)
    const [updateMedAppt] =
        useUpdateMedApptsMutation(id)

    const [apptData, setApptData] = useState({
        id: '',
        date: '',
        location: '',
        reason: '',
        description_of_care: '',
        treatment_plan: '',
        children_id: '',
    })

    useEffect(() => {
        if (appointment) {
            setApptData({
                id: id,
                date: appointment.date,
                location: appointment.location,
                reason: appointment.reason,
                description_of_care: appointment.description_of_care,
                treatment_plan: appointment.treatment_plan,
                children_id: appointment.children_id,
            })
        }
    }, [appointment, id])

    const handleChange = (e) => {
        setApptData({ ...apptData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const updateAppt = { apptid: id, apptData: apptData }
            await updateMedAppt(updateAppt).unwrap()
            navigate(`/child/profile/${apptData.children_id}`)
        } catch (error) {
            console.error('Failed to update appointment:', error)
        }
    }

    if (isLoading) return <div>Loading...</div>

    return (
        <div className="addMedApptForm">
            <h2 className="medApptFormTitle">Edit Medical Appointment</h2>
            <form className="medApptForm" onSubmit={handleSubmit}>
                <div className="medApptInfoForm">
                    <div className="medApptFormInput">
                        <label htmlFor="date">Date</label>
                        <input
                            type="date"
                            name="date"
                            value={apptData.date}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="medApptFormInput">
                        <label htmlFor="location">Location</label>
                        <input
                            type="text"
                            name="location"
                            value={apptData.location}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="medApptFormInput">
                        <label htmlFor="reason">Reason</label>
                        <input
                            type="text"
                            name="reason"
                            value={apptData.reason}
                            onChange={handleChange}
                        />
                    </div>
                </div>
                <div className="medApptFormInput medApptFormTextarea">
                    <label htmlFor="description_of_care">
                        Description of Care
                    </label>
                    <textarea
                        name="description_of_care"
                        value={apptData.description_of_care}
                        onChange={handleChange}
                    />
                </div>
                <div className="medApptFormInput medApptFormTextarea">
                    <label htmlFor="treatment_plan">Treatment Plan</label>
                    <textarea
                        name="treatment_plan"
                        value={apptData.treatment_plan}
                        onChange={handleChange}
                    />
                </div>
                <button type="submit" className="medApptFormBtn">
                    Update Appointment
                </button>
            </form>
        </div>
    )
}

export default MedApptEditForm
