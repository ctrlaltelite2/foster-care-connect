import { useState, useEffect } from 'react'
import { useCreateMedicationMutation } from '../features/Slices/MedicationSlice'
import { useGetChildByIdQuery } from '../features/Slices/ChildSlice'
import { useNavigate, useParams } from 'react-router-dom'
import { useGetTokenQuery } from '../features/auth/authSlice'
import '../css/MedicationForm.css'

const MedicationFormPage = () => {
    const { id } = useParams()
    const childId = parseInt(id)
    const navigate = useNavigate()
    const { data, isLoading: tokenIsLoading } = useGetTokenQuery()
    const {
        data: children,
        isError: childrenError,
        isLoading: childrenLoading,
    } = useGetChildByIdQuery(childId)
    const [createMedication, { isLoading, isError }] =
        useCreateMedicationMutation()

    useEffect(() => {
        if (tokenIsLoading) {
            return
        }

        if (!data) {
            navigate(`/`)
        }
    }, [data, navigate, tokenIsLoading])

    const [medicationData, setMedicationData] = useState({
        name: '',
        dosage: '',
        quantity: '',
        expiration_date: '',
        children_id: id,
    })

    const handleChange = (e) => {
        setMedicationData({
            ...medicationData,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setMedicationData({ ...medicationData, children_id: childId })
        try {
            await createMedication(medicationData).unwrap()
            setMedicationData({
                name: '',
                dosage: '',
                quantity: '',
                expiration_date: '',
            })
            navigate(`/child/profile/${childId}`)
        } catch (error) {
            console.error('Failed to create medication:', error)
            alert('Error creating medication')
        }
    }

    if (childrenLoading) {
        return <div>Loading...</div>
    }
    if (childrenError || !children) {
        return <div>Error loading children</div>
    }

    return (
        <div className="addMedicationForm">
            <h1 className="medicationFormTitle">
                {' '}
                Add A Medication for {children.name}
            </h1>
            <form className="medicationForm" onSubmit={handleSubmit}>
                <div className="medicationInfoForm">
                    <div className="medicationFormInput">
                        <label>Name:</label>
                        <input
                            type="text"
                            name="name"
                            value={medicationData.name}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="medicationFormInput">
                        <label>Dosage:</label>
                        <input
                            type="text"
                            name="dosage"
                            value={medicationData.dosage}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="medicationFormInput">
                        <label>Quantity:</label>
                        <input
                            type="text"
                            name="quantity"
                            value={medicationData.quantity}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="medicationFormInput">
                        <label>Expiration Date:</label>
                        <input
                            type="date"
                            name="expiration_date"
                            value={medicationData.expiration_date}
                            onChange={handleChange}
                        />
                    </div>
                </div>
                <button
                    className="medicationFormBtn"
                    type="submit"
                    disabled={isLoading}
                >
                    Create Medication
                </button>
            </form>
            {isError && <p>Error in creating medication.</p>}
        </div>
    )
}

export default MedicationFormPage
