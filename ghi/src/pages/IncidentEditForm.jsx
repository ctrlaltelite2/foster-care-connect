import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetIncidentsByIdQuery,
    useUpdateIncidentsMutation,
} from '../features/Slices/IncidentSlice'
import '../css/IncidentForm.css'

const IncidentsEditForm = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const {
        data: incidentData,
        isLoading,
        isError,
    } = useGetIncidentsByIdQuery(id)
    const [updateIncident] = useUpdateIncidentsMutation(id)
    const [isChecked, setIsChecked] = useState(incidentData?.police_involved)
    const [setErrorMessage] = useState('')
    const [formData, setFormData] = useState({
        id: '',
        name: '',
        picture: '',
        description: '',
        police_involved: false,
        action_plan: '',
        children_id: '',
    })

    useEffect(() => {
        if (incidentData) {
            setIsChecked(incidentData.police_involved)
            setFormData({
                id: id,
                name: incidentData.name,
                picture: incidentData.picture,
                description: incidentData.description,
                police_involved: incidentData.police_involved,
                action_plan: incidentData.action_plan,
                children_id: incidentData.children_id,
            })
        }
    }, [incidentData, id])

    const handleCheckedChange = (e) => {
        const checked = e.target.checked
        setIsChecked(checked)
        setFormData({ ...formData, police_involved: checked })
    }

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const updatedData = { IncidentId: id, ...formData }
            await updateIncident(updatedData).unwrap()
            navigate(`/child/profile/${incidentData.children_id}`)
        } catch (error) {
            setErrorMessage('Failed to update the incident')
        }
    }

    if (isLoading) return <div>Loading...</div>
    if (isError) return <div>Error loading incident data</div>

    return (
        <>
            <div className="addIncidentForm">
                <h2 className="incidentFormTitle">Edit Incident Report</h2>
                <form className="incidentForm" onSubmit={handleSubmit}>
                    <div>
                        <div className="incidentInfoForm">
                            <div className="incidentFormInput">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    value={formData.name}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="incidentFormInput">
                                <label htmlFor="description">Description</label>
                                <input
                                    type="text"
                                    name="description"
                                    id="description"
                                    value={formData.description}
                                    onChange={handleChange}
                                />
                            </div>

                            <div className="incidentFormInput">
                                <label htmlFor="picture">Upload Image</label>
                                <input
                                    type="text"
                                    name="picture"
                                    id="picture"
                                    value={formData.picture}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className="incidentFormInput incidentFormTextarea">
                            <label htmlFor="action_plan">Action Plan</label>
                            <textarea
                                id="action_plan"
                                name="action_plan"
                                value={formData.action_plan}
                                onChange={handleChange}
                            ></textarea>
                        </div>
                        <div className="incidentFormCheck">
                            <label htmlFor="police_involved">
                                <p>
                                    Police Involved? {isChecked ? 'Yes' : 'No'}.
                                </p>
                            </label>
                            <input
                                type="checkbox"
                                name="police_involved"
                                checked={isChecked}
                                id="police_involved"
                                value={formData.police_involved}
                                onChange={handleCheckedChange}
                            />
                        </div>
                    </div>
                    <button
                        type="submit"
                        className="incidentFormBtn"
                        disabled={isLoading}
                    >
                        Submit
                    </button>
                </form>
                {isError && <p>Something has gone wrong</p>}
            </div>
        </>
    )
}

export default IncidentsEditForm
