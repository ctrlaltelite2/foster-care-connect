import '../css/AgentProfile.css'
import { useState } from 'react'
import { useParams, useNavigate, Link } from 'react-router-dom'
import { useGetAgentByIdQuery } from '../features/Slices/AgentSlice'
import { useGetTokenQuery } from '../features/auth/authSlice'
import { useGetAllChildrenQuery } from '../features/Slices/ChildSlice'
import { useGetAllParentsQuery } from '../features/Slices/ParentSlice'
import AgentChildCard from '../components/AgentChildCard'
import ParentCard from '../components/ParentCard'
import defaultProfile from '../assets/defaultProfile.jpg'

const AgentProfile = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const { data: agent, isError: agentError } = useGetAgentByIdQuery(id)
    const { data: tokenData } = useGetTokenQuery()
    const { data: parents, isLoading: parentsLoading } = useGetAllParentsQuery()
    const { data: children, isLoading: childrenIsLoading } =
        useGetAllChildrenQuery()
    const [filterType, setFilterType] = useState('')
    const [allParents, setAllParents] = useState(false)
    const [allChildren, setAllChildren] = useState(true)

    if (childrenIsLoading || parentsLoading || parentsLoading)
        return <div>Loading...</div>
    if (agentError || !agent) return <div>Error loading profile.</div>

    if (tokenData.account.id !== agent.account_id) {
        navigate(`/`)
        return null
    }

    function handleFilterTypeChange(e) {
        setFilterType(e.target.value)
    }

    const handleProfileUpdate = () => {
        navigate(`/agent/editprofile/${id}`)
    }

    const showParents = () => {
        setAllChildren(false)
        setAllParents(true)
    }

    const showChildren = () => {
        setAllChildren(true)
        setAllParents(false)
    }

    return (
        <div className="agent-profile">
            <h1 className="agent-profile-welcome">Welcome {agent.name}</h1>
            <div className="agent-profile-section">
                <div className="agent-profile-pic-info">
                    <div className="agent-profile-pic-update">
                        <img
                            src={
                                agent.profile_picture !== ''
                                    ? agent.profile_picture
                                    : defaultProfile
                            }
                            alt={agent.name}
                            className="agent-profile-pic"
                        />
                        <button
                            className="agent-profile-updatebtn"
                            onClick={handleProfileUpdate}
                        >
                            Update Profile
                        </button>
                    </div>
                </div>
                <div className="agent-profile-details">
                    <p>{agent.name}</p>
                    <p>{agent.employee_id}</p>
                </div>
                <div className="agentProfileContact">
                    <p>
                        <Link to={'/child-signup'}>Add a Child</Link>
                    </p>
                    <p>
                        <Link to={'/parent-signup'}>Add a Parent</Link>
                    </p>
                    <p className="agentProfileEmail">
                        <Link to={'/agent-signup'}>Add a Agent</Link>
                    </p>
                </div>
            </div>
            <div className="agent-Buttons">
                <button
                    className={`agent-showParents ${
                        allParents && 'underlined'
                    }`}
                    onClick={showParents}
                >
                    Parents
                </button>
                <button
                    className={`agent-showParents ${
                        !allParents && 'underlined'
                    }`}
                    onClick={showChildren}
                >
                    Children
                </button>
            </div>

            {allChildren && (
                <>
                    <div className="agent-select-div">
                        <select
                            className="agent-select"
                            onChange={handleFilterTypeChange}
                        >
                            <option value="">Choose a parent...</option>
                            {parents.map((parent) => {
                                return (
                                    <option key={parent.id} value={parent.id}>
                                        {parent.parent1_name} and{' '}
                                        {parent.parent2_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="children-of-agent">
                        <ul className="agent-profile-child-grid">
                            {parents &&
                                (filterType
                                    ? children
                                          .filter(
                                              (child) =>
                                                  child.parents_id ===
                                                  parseInt(filterType)
                                          )
                                          .map((child) => (
                                              <li
                                                  className="agent-child-card"
                                                  key={child.id}
                                              >
                                                  <Link
                                                      to={`/child/profile/${child.id}`}
                                                  >
                                                      <AgentChildCard
                                                          child={child}
                                                          parents={parents}
                                                      />
                                                  </Link>
                                              </li>
                                          ))
                                    : children.map((child) => (
                                          <li
                                              className="agent-child-card"
                                              key={child.id}
                                          >
                                              <Link
                                                  to={`/child/profile/${child.id}`}
                                              >
                                                  <AgentChildCard
                                                      child={child}
                                                      parents={parents}
                                                  />
                                              </Link>
                                          </li>
                                      )))}
                        </ul>
                    </div>
                </>
            )}

            {allParents && (
                <div className="children-of-agent">
                    <ul className="agent-profile-child-grid">
                        {parents.map((parent) => (
                            <li className="agent-child-card" key={parent.id}>
                                <ParentCard parent={parent} />
                            </li>
                        ))}
                    </ul>
                </div>
            )}
        </div>
    )
}

export default AgentProfile
