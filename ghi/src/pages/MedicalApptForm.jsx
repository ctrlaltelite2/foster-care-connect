import { useState, useEffect } from 'react'
import { useCreateMedApptsMutation } from '../features/Slices/medApptSlice'
import { useGetChildByIdQuery } from '../features/Slices/ChildSlice'
import { useNavigate, useParams } from 'react-router-dom'
import { useGetTokenQuery } from '../features/auth/authSlice'
import '../css/MedicalApptForm.css'

const MedApptForm = () => {
    const { id } = useParams()
    const childId = parseInt(id)
    const navigate = useNavigate()
    const [createMedAppt, { isLoading, isError }] = useCreateMedApptsMutation()
    const { data, isLoading: tokenIsLoading } = useGetTokenQuery()

    const {
        data: children,
        isError: childrenError,
        isLoading: childrenLoading,
    } = useGetChildByIdQuery(childId)

    useEffect(() => {
        if (tokenIsLoading) {
            return
        }

        if (!data) {
            navigate(`/child/profile/${id}`)
        }
    }, [data, navigate, tokenIsLoading, id])

    const [apptData, setApptData] = useState({
        date: '',
        location: '',
        reason: '',
        description_of_care: '',
        treatment_plan: '',
        children_id: childId,
    })

    const handleChange = (e) => {
        setApptData({ ...apptData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setApptData({ ...apptData, children_id: childId })
        try {
            await createMedAppt(apptData).unwrap()
            setApptData({
                date: '',
                location: '',
                reason: '',
                description_of_care: '',
                treatment_plan: '',
            })
            navigate(`/child/profile/${childId}`)
        } catch (error) {
            console.error('Failed to create appointment:', error)
            alert('Something Went Wrong')
        }
    }

    if (childrenLoading) {
        return <div>Loading...</div>
    }
    if (childrenError || !children) {
        return <div>Error loading children</div>
    }

    return (
        <>
            <div className="addMedApptForm">
                <h2 className="medApptFormTitle">
                    Add a Medical Appointment for {children.name}
                </h2>
                <form className="medApptForm" onSubmit={handleSubmit}>
                    <div className="medApptInfoForm">
                        <div className="medApptFormInput">
                            <label htmlFor="date">Date</label>
                            <input
                                type="date"
                                placeholder="Date"
                                name="date"
                                value={apptData.date}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="medApptFormInput">
                            <label htmlFor="location">Location</label>
                            <input
                                type="text"
                                name="location"
                                placeholder="location"
                                value={apptData.location}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="medApptFormInput">
                            <label htmlFor="reason">Reason</label>
                            <input
                                type="text"
                                name="reason"
                                placeholder="Reason for Visit"
                                value={apptData.reason}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="medApptFormInput medApptFormTextarea">
                        <label htmlFor="description_of_care">
                            Description of Care
                        </label>
                        <textarea
                            placeholder="Description of Care"
                            name="description_of_care"
                            value={apptData.description_of_care}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="medApptFormInput medApptFormTextarea">
                        <label htmlFor="treatment_plan">Treatment Plan</label>
                        <textarea
                            placeholder="Plan for Treatment"
                            name="treatment_plan"
                            value={apptData.treatment_plan}
                            onChange={handleChange}
                        />
                    </div>
                    <button
                        type="submit"
                        className="medApptFormBtn"
                        disabled={isLoading}
                    >
                        Submit
                    </button>
                </form>
                {isError && <p>Something has gone wrong</p>}
            </div>
        </>
    )
}

export default MedApptForm
