import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetChildByIdQuery,
    useUpdateChildMutation,
} from '../features/Slices/ChildSlice'
import { useGetAllParentsQuery } from '../features/Slices/ParentSlice'
import { useGetAllAgentsQuery } from '../features/Slices/AgentSlice'
import '../css/ChildEditProfile.css'

const EditChildProfile = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const { data: childData, isLoading, isError } = useGetChildByIdQuery(id)
    const [updateChild, { isLoading: isUpdating }] = useUpdateChildMutation(id)
    const [errorMessage, setErrorMessage] = useState('')
    const { data: parents, isLoading: parentsLoading } = useGetAllParentsQuery()
    const { data: agents, isLoading: agentsLoading } = useGetAllAgentsQuery()
    const [selectedParent, setSelectedParent] = useState('')
    const [selectedAgent, setSelectedAgent] = useState('')

    const [formData, setFormData] = useState({
        id: id,
        profile_picture: '',
        name: '',
        age: '',
        parents_id: '',
        agents_id: '',
    })

    useEffect(() => {
        if (childData) {
            setFormData((prevFormData) => ({
                ...prevFormData,
                id: id,
                profile_picture: childData.profile_picture,
                name: childData.name,
                age: childData.age,
                parents_id: childData.parents_id,
                agents_id: childData.agents_id,
            }));
            setSelectedParent(childData.parents_id || '');
            setSelectedAgent(childData.agents_id || '');
        }
    }, [childData, id])

    useEffect(() => {
        setFormData((prevFormData) => ({ ...prevFormData, parents_id: selectedParent }))
    }, [selectedParent, childData])

    useEffect(() => {
        setFormData((prevFormData) => ({ ...prevFormData, agents_id: selectedAgent }))
    }, [selectedAgent, childData])

    const handleInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await updateChild({ childId: id, ...formData }).unwrap()
            navigate(`/child/profile/${id}`)
        } catch (error) {
            setErrorMessage('Failed to update profile')
        }
    }

    if (isLoading) return <div>Loading...</div>
    if (isError) return <div>Error loading profile data</div>
    if (parentsLoading || agentsLoading) return <div>Loading...</div>

    return (
        <div className="childEditPage">
            <h1 className="childEditPageTitle">Edit Child&apos;s Profile</h1>
            {errorMessage && (
                <div className="alert alert-danger">{errorMessage}</div>
            )}
            <form className="childEditForm" onSubmit={handleSubmit}>
                <div className="childEditInfo">
                    <p className="childEditPageSubitle">Child Information</p>
                    <div className="childEditInfoForm">
                        <div className="childEditInput">
                            <label>Profile Picture URL:</label>
                            <input
                                type="text"
                                name="profile_picture"
                                id="profile_picture"
                                value={formData.profile_picture}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="childEditInput">
                            <label>Name:</label>
                            <input
                                type="text"
                                name="name"
                                id="name"
                                value={formData.name}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="childEditInput">
                            <label>Age:</label>
                            <input
                                type="text"
                                name="age"
                                id="age"
                                value={formData.age}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="childSignupInput">
                            <select
                                onChange={(e) =>
                                    setSelectedParent(e.target.value)
                                }
                                name="parents_id"
                                value={selectedParent}
                                className="child-select"
                            >
                                <option value="">Parents</option>
                                {parents.map((parent) => {
                                    return (
                                        <option
                                            key={parent.id}
                                            value={parent.id}
                                        >
                                            {parent.parent1_name} and&nbsp;
                                            {parent.parent2_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="childSignupInput">
                            <select
                                onChange={(e) =>
                                    setSelectedAgent(e.target.value)
                                }
                                name="agents_id"
                                value={selectedAgent}
                                className="child-select"
                            >
                                <option value="">Agent</option>
                                {agents.map((agent) => {
                                    return (
                                        <option key={agent.id} value={agent.id}>
                                            {agent.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="childEditBtn"
                    disabled={isUpdating}
                >
                    Update Profile
                </button>
            </form>
            {/* <button className="childEditBtn" onClick={handleDelete}>
                Delete Profile
            </button> */}
        </div>
    )
}

export default EditChildProfile
