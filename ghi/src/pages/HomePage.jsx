import '../css/HomePage.css'
import fosterHands from '../assets/fosterHands.jpg'
import parentLogo from '../assets/parentLogo.png'
import ServicesLogo from '../assets/SevicesLogo.png'
import child from '../assets/child.jpg'
import agent from '../assets/agent.jpg'
import parent from '../assets/parent.jpg'
import { Link } from 'react-router-dom'

function MainPage() {
    return (
        <div className="homePage">
            <div className="homePageCoverimg">
                <img
                    className="homePageCoverimg"
                    src={fosterHands}
                    alt="Home Page cover photo hands holding a paper house"
                />
            </div>
            <div className="homePageButtons">
                <Link className="homeButton" to={"/parent-login"}>
                    <img className="homepageLogos" src={parentLogo} alt="" />
                    <p>Parents</p>
                </Link>
                <Link className="homeButton1" to={"/agent-login"}>
                    <img className="homepageLogos" src={ServicesLogo} alt="" />
                    <p>Agents</p>
                </Link>
            </div>
            <div className="homePageBanner">
                <h1>What&apos;s Inside!</h1>
            </div>
            <div className="homePageImgs">
                <img src={child} alt="" />
                <img src={parent} alt="" />
                <img src={agent} alt="" />
            </div>
            <div className="homePageDescriptions">
                <p className="homePageDescription1">
                    Have all your child&apos;s information on one convenient
                    page! Update and track all needed information in their
                    dashboard
                </p>
                <p className="homePageDescription2">
                    Family dashboard allows parents and agents to see all family
                    unit information in one easy place!
                </p>
                <p className="homePageDescription3">
                    Agent profile gives agents a dashboard to be able to access
                    all children under their care with the click of a picture!
                </p>
            </div>
            <div className="homePageSlogan">
                <p>Simplifying the Paperwork</p>
            </div>
        </div>
    )
}

export default MainPage
