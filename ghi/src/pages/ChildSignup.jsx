import { useState, useEffect } from 'react'
import { useCreateChildMutation } from '../features/Slices/ChildSlice'
import { useNavigate } from 'react-router-dom'
import { useGetTokenQuery } from '../features/auth/authSlice'
import { useGetAllParentsQuery } from '../features/Slices/ParentSlice'
import { useGetAllAgentsQuery } from '../features/Slices/AgentSlice'
import '../css/ChildSignup.css'

const ChildSignupPage = () => {
    const navigate = useNavigate()
    const { data, isLoading: tokenIsLoading } = useGetTokenQuery()
    const [isLoading, setIsLoading] = useState(false)
    const { data: parents, isLoading: parentsLoading } = useGetAllParentsQuery()
    const { data: agents, isLoading: agentsLoading } = useGetAllAgentsQuery()
    const [selectedParent, setSelectedParent] = useState('')
    const [selectedAgent, setSelectedAgent] = useState('')
    const [childData, setChildData] = useState({
        name: '',
        age: '',
        profile_picture: '',
        parents_id: '',
        agents_id: '',
    })
    const [createChild, { isError }] = useCreateChildMutation()

    useEffect(() => {
        if (tokenIsLoading) {
            return
        }

        if (!data) {
            navigate(`/`)
        }
    }, [data, navigate, tokenIsLoading])

    useEffect(() => {
        setChildData((prevData) => ({
            ...prevData,
            parents_id: selectedParent,
            agents_id: selectedAgent,
        }));
    }, [selectedParent, selectedAgent])

    const handleChange = (e) => {
        setChildData({ ...childData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setIsLoading(true)

        try {
            const createdChild = await createChild(childData).unwrap()
            setChildData({
                name: '',
                age: '',
                profile_picture: '',
                parents_id: '',
                agents_id: '',
            })
            navigate(`/child/profile/${createdChild.id}`)
        } catch (error) {
            console.error('Failed to create child:', error)
        } finally {
            setIsLoading(false)
        }
    }

    if (parentsLoading || agentsLoading) return <div>Loading...</div>

    return (
        <div className="childSignupPage">
            <h1 className="childSignupPageTitle">Child Signup</h1>
            <form className="childSignupInfoForm" onSubmit={handleSubmit}>
                <div className="childSignupInput">
                    <label>Name:</label>
                    <input
                        type="text"
                        name="name"
                        value={childData.name}
                        onChange={handleChange}
                    />
                </div>
                <div className="childSignupInput">
                    <label>Age:</label>
                    <input
                        type="number"
                        name="age"
                        value={childData.age}
                        onChange={handleChange}
                    />
                </div>
                <div className="childSignupInput">
                    <label>Profile Picture URL:</label>
                    <input
                        type="text"
                        name="profile_picture"
                        value={childData.profile_picture}
                        onChange={handleChange}
                    />
                </div>
                <div className="childSignupInput">
                    <select
                        onChange={(e) => setSelectedParent(e.target.value)}
                        name="parents_id"
                        value={selectedParent}
                        className="child-select"
                    >
                        <option>Parents</option>
                        {parents.map((parent) => {
                            return (
                                <option key={parent.id} value={parent.id}>
                                    {parent.parent1_name} and&nbsp;
                                    {parent.parent2_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="childSignupInput">
                    <select
                        onChange={(e) => setSelectedAgent(e.target.value)}
                        name="agents_id"
                        value={selectedAgent}
                        className="child-select"
                    >
                        <option>Agent</option>
                        {agents.map((agent) => {
                            return (
                                <option key={agent.id} value={agent.id}>
                                    {agent.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button
                    type="submit"
                    className="childSignupBtn"
                    disabled={isLoading}
                >
                    Create Child
                </button>
            </form>
            {isError && <p>Error in creating child.</p>}
        </div>
    )
}

export default ChildSignupPage
