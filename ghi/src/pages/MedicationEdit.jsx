import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetMedicationByIdQuery,
    useUpdateMedicationMutation,
} from '../features/Slices/MedicationSlice'
import '../css/MedicationEdit.css'

const EditMedicationForm = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const {
        data: medicationData,
        isLoading,
        isError,
    } = useGetMedicationByIdQuery(id)
    const [updateMedication, { isLoading: isUpdating }] =
        useUpdateMedicationMutation(id)
    const [errorMessage, setErrorMessage] = useState('')

    const [formData, setFormData] = useState({
        id: id,
        name: '',
        dosage: '',
        quantity: '',
        expiration_date: '',
        children_id: '',
    })

    useEffect(() => {
        if (medicationData) {
            setFormData({
                id: id,
                name: medicationData.name,
                dosage: medicationData.dosage,
                quantity: medicationData.quantity,
                expiration_date: medicationData.expiration_date,
                children_id: medicationData.children_id,
            })
        }
    }, [medicationData, id])

    const handleInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await updateMedication({ medicationId: id, ...formData }).unwrap()
            navigate(`/child/profile/${medicationData.children_id}`)
        } catch (error) {
            setErrorMessage('Fail to update Medication')
        }
    }

    if (isLoading) return <div>Loading...</div>
    if (isError) return <div>Error loading profile data</div>

    return (
        <div className="medicationEditPage">
            <h1 className="medicationEditPageTitle">
                Edit Medication Information
            </h1>
            {errorMessage && (
                <div className="alert alert-danger">{errorMessage}</div>
            )}
            <form className="medicationEditForm" onSubmit={handleSubmit}>
                <div className="medicationEditInfo">
                    <p className="medicationEditPageSubitle">
                        Medication Information
                    </p>
                    <div className="medicationEditInfoForm">
                        <div className="medicationEditInput">
                            <label>Name:</label>
                            <input
                                type="text"
                                name="name"
                                id="name"
                                value={formData.name}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="medicationEditInput">
                            <label>Dosage:</label>
                            <input
                                type="text"
                                name="dosage"
                                id="dosage"
                                value={formData.dosage}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="medicationEditInput">
                            <label>Quantity:</label>
                            <input
                                type="text"
                                name="quantity"
                                id="quantity"
                                value={formData.quantity}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="medicationEditInput">
                            <label>Expiration Date:</label>
                            <input
                                type="text"
                                name="expiration_date"
                                id="expiration_date"
                                value={formData.expiration_date}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="medicationEditBtn"
                    disabled={isUpdating}
                >
                    Update Medication
                </button>
            </form>
            {/* <button className="medicationEditBtn" onClick={handleDelete}>
                Delete Profile
            </button> */}
        </div>
    )
}

export default EditMedicationForm
