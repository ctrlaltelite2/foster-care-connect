import { useState, useEffect } from 'react'
import { useCreateToyMutation } from '../features/Slices/ToySlice'
import { useGetChildByIdQuery } from '../features/Slices/ChildSlice'
import { useGetTokenQuery } from '../features/auth/authSlice'
import { useNavigate, useParams } from 'react-router-dom'
import '../css/ToyForm.css'

const ToyForm = () => {
    const { id } = useParams()
    const childId = parseInt(id)
    const navigate = useNavigate()
    const [createToy, { isLoading, isError }] = useCreateToyMutation()
    const { data, isLoading: tokenIsLoading } = useGetTokenQuery()
    const {
        data: children,
        isError: childrenError,
        isLoading: childrenLoading,
    } = useGetChildByIdQuery(childId)

    useEffect(() => {
        if (tokenIsLoading) {
            return
        }

        if (!data) {
            navigate(`/`)
        }
    }, [data, navigate, tokenIsLoading])

    const [toyData, setToyData] = useState({
        date: '',
        name: '',
        picture: '',
        description: '',
        condition: '',
        children_id: childId,
    })

    const handleChange = (e) => {
        setToyData({ ...toyData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setToyData({ ...toyData, children_id: childId })
        try {
            await createToy(toyData).unwrap()
            setToyData({
                date: '',
                name: '',
                picture: '',
                description: '',
                condition: '',
            })
            navigate(`/child/profile/${childId}`)
        } catch (error) {
            console.error('Failed to add toy:', error)
            alert('Something Went Wrong')
        }
    }

    if (childrenLoading) {
        return <div>Loading...</div>
    }
    if (childrenError || !children) {
        return <div>Error loading children</div>
    }

    return (
        <>
            <div className="addToyForm">
                <h2 className="toyFormTitle">Add A Toy for {children.name}</h2>
                <form className="toyForm" onSubmit={handleSubmit}>
                    <div className="toyInfoForm">
                        <div className="toyFormInput">
                            <label>Date</label>
                            <input
                                type="date"
                                name="date"
                                placeholder="Date"
                                value={toyData.date}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="toyFormInput">
                            <label>Name</label>
                            <input
                                type="text"
                                name="name"
                                placeholder="Toy Name"
                                value={toyData.name}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="toyFormInput">
                            <label>Image</label>
                            <input
                                type="text"
                                name="picture"
                                value={toyData.picture}
                                onChange={handleChange}
                                placeholder="Image URL"
                            />
                        </div>
                    </div>
                    <div className="toyFormInput toyFormTextarea">
                        <label>Condition</label>
                        <textarea
                            name="condition"
                            placeholder="Condition of toy"
                            value={toyData.condition}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="toyFormInput toyFormTextarea">
                        <label htmlFor="description">Description</label>
                        <textarea
                            name="description"
                            placeholder="Description"
                            value={toyData.description}
                            onChange={handleChange}
                        />
                    </div>
                    <button
                        type="submit"
                        className="toyFormBtn"
                        disabled={isLoading}
                    >
                        {isLoading ? 'Adding...' : 'Add Toy'}
                    </button>
                </form>
                {isError && <p>Something has gone wrong</p>}
            </div>
        </>
    )
}

export default ToyForm
