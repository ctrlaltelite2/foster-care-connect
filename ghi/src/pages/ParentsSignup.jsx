import { useState, useEffect, useCallback } from 'react'
import { useSignupMutation } from '../features/auth/authSlice'
import { useCreateParentMutation } from '../features/Slices/ParentSlice'
import { useNavigate } from 'react-router-dom'
import '../css/ParentSignup.css'

const ParentSignup = () => {
    const [signup, { isSuccess, isError, error, data }] = useSignupMutation()
    const [createParent, { isLoading: isParentLoading }] =
        useCreateParentMutation()

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [parentData, setParentData] = useState({
        parent1_name: '',
        parent2_name: '',
        location: '',
        account_id: '',
        profile_picture: '',
    })
    const [errorMessage, setErrorMessage] = useState('')
    const navigate = useNavigate()
    const createParentProfile = useCallback(
        async (accountId) => {
            try {
                const parentProfileData = {
                    ...parentData,
                    account_id: accountId,
                }
                await createParent(parentProfileData).unwrap()
                navigate('/parent-login')
            } catch (error) {
                console.error('Failed to create parent:', error)
                setErrorMessage(
                    error.data?.detail || 'Error creating parent profile'
                )
            }
        },
        [createParent, navigate, parentData]
    )

    const createParentProfileCallback = useCallback(
        () => createParentProfile(data?.account?.id),
        [data?.account?.id, createParentProfile]
    )

    useEffect(() => {
        if (isSuccess) {
            const accountId = data?.account?.id
            if (accountId) {
                createParentProfileCallback(accountId)
            }
        } else if (isError) {
            setErrorMessage(
                error?.data?.detail || 'An unexpected signup error occurred'
            )
        }
    }, [isSuccess, isError, error, data, createParentProfileCallback])

    const handleSignupSubmit = (e) => {
        e.preventDefault()
        if (password !== confirmPassword) {
            setErrorMessage('Passwords do not match')
            return
        }
        signup({ username, password })
    }

    const handleParentDataChange = (e) => {
        setParentData({ ...parentData, [e.target.name]: e.target.value })
    }

    return (
        <div className="parentSignupPage">
            <h1 className="parentSignupPageTitle">Add A New Family</h1>
            {errorMessage && <div role="alert">{errorMessage}</div>}
            <form className="parentSignupForm" onSubmit={handleSignupSubmit}>
                <div className="parentSignupAccountInfo">
                    <p className="parentSignupPageSubitle">
                        Account Information
                    </p>
                    <div className="parentSignupAccountInfoForm">
                        <div className="parentSignupInput">
                            <label htmlFor="Signup_username">Username</label>
                            <input
                                type="text"
                                id="Signup_username"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>
                        <div className="parentSignupInput">
                            <label htmlFor="Signup_password">Password</label>
                            <input
                                type="password"
                                id="Signup_password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <div className="parentSignupInput">
                            <label htmlFor="Signup_confirmPassword">
                                Confirm Password
                            </label>
                            <input
                                type="password"
                                id="Signup_confirmPassword"
                                value={confirmPassword}
                                onChange={(e) =>
                                    setConfirmPassword(e.target.value)
                                }
                            />
                        </div>
                    </div>
                </div>
                <div className="parentSignupFamilyInfo">
                    <p className="parentSignupPageSubitle">
                        Family Information
                    </p>
                    <div className="parentSignupFamilyInfoForm">
                        <div className="parentSignupInput">
                            <label>Parent 1 Name:</label>
                            <input
                                type="text"
                                name="parent1_name"
                                value={parentData.parent1_name}
                                onChange={handleParentDataChange}
                            />
                        </div>
                        <div className="parentSignupInput">
                            <label>Parent 2 Name:</label>
                            <input
                                type="text"
                                name="parent2_name"
                                value={parentData.parent2_name}
                                onChange={handleParentDataChange}
                            />
                        </div>
                        <div className="parentSignupInput">
                            <label>Location:</label>
                            <input
                                type="text"
                                name="location"
                                value={parentData.location}
                                onChange={handleParentDataChange}
                            />
                        </div>
                        <div className="parentSignupInput">
                            <label>Profile Picture URL:</label>
                            <input
                                type="text"
                                name="profile_picture"
                                value={parentData.profile_picture}
                                onChange={handleParentDataChange}
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="parentSignupBtn"
                    disabled={isParentLoading}
                >
                    Sign Up
                </button>
            </form>
        </div>
    )
}

export default ParentSignup
