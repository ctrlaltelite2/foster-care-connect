import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetToyByIdQuery,
    useUpdateToyMutation,
} from '../features/Slices/ToySlice'
import '../css/ToyForm.css'

const EditToyForm = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const {
        data: toyData,
        isLoading: toyLoading,
        isError: toyError,
    } = useGetToyByIdQuery(id)
    const [updateToy, { isLoading, isError }] = useUpdateToyMutation()

    const [formData, setFormData] = useState({
        date: '',
        name: '',
        picture: '',
        description: '',
        condition: '',
        children_id: '',
    })

    useEffect(() => {
        if (toyData) {
            setFormData({
                date: toyData.date || '',
                name: toyData.name || '',
                picture: toyData.picture || '',
                description: toyData.description || '',
                condition: toyData.condition || '',
                children_id: toyData.children_id || '',
            })
        }
    }, [toyData])

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await updateToy({ toyId: id, ...formData }).unwrap()
            navigate(`/child/profile/${formData.children_id}`)
        } catch (error) {
            console.error('Failed to update toy:', error)
        }
    }

    if (toyLoading) return <div>Loading...</div>
    if (toyError || !toyData) return <div>Error loading toy</div>

    return (
        <div className="addToyForm">
            <h2 className="toyFormTitle">Edit Toy {toyData?.name}</h2>
            <form className="toyForm" onSubmit={handleSubmit}>
                <div className="toyInfoForm">
                    <div className="toyFormInput">
                        <label>Date</label>
                        <input
                            type="date"
                            name="date"
                            value={formData.date}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="toyFormInput">
                        <label>Name</label>
                        <input
                            type="text"
                            name="name"
                            value={formData.name}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="toyFormInput">
                        <label>Image</label>
                        <input
                            type="text"
                            name="picture"
                            value={formData.picture}
                            onChange={handleChange}
                        />
                    </div>
                </div>
                <div className="toyFormInput toyFormTextarea">
                    <label>Condition</label>
                    <textarea
                        name="condition"
                        value={formData.condition}
                        onChange={handleChange}
                    />
                </div>
                <div className="toyFormInput toyFormTextarea">
                    <label>Description</label>
                    <textarea
                        name="description"
                        value={formData.description}
                        onChange={handleChange}
                    />
                </div>
                <button
                    type="submit"
                    className="toyFormBtn"
                    disabled={isLoading}
                >
                    {isLoading ? 'Updating...' : 'Update Toy'}
                </button>
            </form>
            {isError && <p>Something has gone wrong</p>}
        </div>
    )
}

export default EditToyForm
