import { Link } from 'react-router-dom'
import kayleeHeadshot from '../assets/kayleeHeadshot.png'
import aaronPhoto from '../assets/aaronPhoto.jpg'
import alexPhoto from '../assets/alexPhoto.jpg'
import AnnaAndMajor from '../assets/AnnaAndMajor.jpeg'
import '../css/AboutPage.css'

function AboutPage() {
    return (
        <div className="aboutPage">
            <div className="aboutPageBanner">
                <h1>What is FosterCare Connect?</h1>
            </div>
            <div className="aboutPageDescription">
                <p>
                    Embark on a tech-tastic journey with FosterCare Connect our
                    brainchild born from a dazzling mix of creativity and coding
                    magic! Picture this: a vibrant web application brought to
                    life using the dynamic duo of Figma for a splash of design
                    pizzazz, Python and FastAPI as the backstage maestros, and
                    React with Vite stealing the show on the frontend runway.
                </p>
                <p>
                    What does it do, you ask? Well, imagine a world where child
                    services agents and parents waltz through mandatory
                    reporting needs effortlessly. This web marvel is more than
                    just a platform, it&apos;s a symphony of seamless
                    communication, turning mandatory reporting into a breeze for
                    foster children.
                </p>
                <p>
                    Hold onto your hats! We&apos;ve spiced things up with
                    real-time data synchronization using Redux Toolkit, ensuring
                    your experience is as snappy as a tap-dancing penguin. And
                    behind the scenes, PostgreSQL plays the role of the guardian
                    of information, making sure everything is as sturdy as a
                    well-built Lego castle.
                </p>
                <p>
                    But that&apos;s not all! We&apos;ve flexed our full-stack
                    muscles using a fusion of JavaScript, CSS, and other
                    cutting-edge tech wonders. The result? A Foster Care Connect
                    website that&apos;s not just user-friendly, it&apos;s the
                    superhero cape for child services agents and parents,
                    effortlessly tracking and fulfilling mandatory reporting
                    requirements, adding a dash of accountability and a sprinkle
                    of improved communication to the foster care system. Get
                    ready to click, connect, and conquer the world of Foster
                    Care Connect!
                </p>
            </div>
            <div className="aboutPageBanner">
                <h1>Meet the Creators!</h1>
            </div>
            <div className="creatorCardGrid">
                <div className="creatorCard">
                    <img
                        src={kayleeHeadshot}
                        className="creatorCardPicture"
                        alt="Photo not found"
                    />
                    <div className="creatorCardInfo">
                        <h3 className="creatorName">Kaylee Harding</h3>
                        <p>
                            Software engineer, Coast Guard Veteran, proud mama
                            of a mini horse named Jellybean
                        </p>
                        <p>
                            <Link
                                to={
                                    'https://www.linkedin.com/in/kayleeharding/'
                                }
                                target="_blank"
                            >
                                linkedin
                            </Link>
                        </p>
                        <p>
                            <Link
                                to={'https://kayleeharding.com/'}
                                target="_blank"
                            >
                                Portfolio
                            </Link>
                        </p>
                        <p>
                            <Link
                                to={'https://gitlab.com/Kaylee2319'}
                                target="_blank"
                            >
                                GitLab
                            </Link>
                        </p>
                        <p>Email me at kayleegharding@gmail.com</p>
                    </div>
                </div>
                <div className="creatorCard">
                    <img
                        src={aaronPhoto}
                        className="creatorCardPicture"
                        alt="Photo not found"
                    />
                    <div className="creatorCardInfo">
                        <h3 className="creatorName">Aaron Straight</h3>
                        <p>
                            Software engineer, Marine Corps Veteran, new father,
                            and avid hunter
                        </p>
                        <p>
                            <Link
                                to={
                                    'https://www.linkedin.com/in/aaron-straight1/'
                                }
                                target="_blank"
                            >
                                linkedin
                            </Link>
                        </p>
                        <p>
                            <Link
                                to={'https://gitlab.com/A-Straight'}
                                target="_blank"
                            >
                                GitLab
                            </Link>
                        </p>
                        <p>Email me at aaron.straight1@gmail.com</p>
                    </div>
                </div>
                <div className="creatorCard">
                    <img
                        src={alexPhoto}
                        className="creatorCardPicture"
                        alt="Photo not found"
                    />
                    <div className="creatorCardInfo">
                        <h3 className="creatorName">Alexander Wilson</h3>
                        <p>
                            Software engineer, Army Veteran, Navy Reservist,
                            unabashed nerd
                        </p>
                        <p>
                            <Link
                                to={
                                    'https://www.linkedin.com/in/alexander-wilson-a59469290/'
                                }
                                target="_blank"
                            >
                                linkedin
                            </Link>
                        </p>
                        <p>
                            <Link
                                to={'https://gitlab.com/QuandoRondo'}
                                target="_blank"
                            >
                                GitLab
                            </Link>
                        </p>
                        <p>Email me at arwilson27@yahoo.com</p>
                    </div>
                </div>
                <div className="creatorCard">
                    <img
                        src={AnnaAndMajor}
                        className="creatorCardPicture"
                        alt="Photo not found"
                    />
                    <div className="creatorCardInfo">
                        <h3 className="creatorName">Adrianna Jones</h3>
                        <p>
                            Software enngineer, Navy Veteran, believes the best
                            adventures can be found in a book
                        </p>
                        <p>
                            <Link
                                to={
                                    'https://www.linkedin.com/in/adrianna-jones-alj/'
                                }
                                target="_blank"
                            >
                                linkedin
                            </Link>
                        </p>
                        <p>
                            <Link
                                to={'https://gitlab.com/adrianna.l.jones1'}
                                target="_blank"
                            >
                                GitLab
                            </Link>
                        </p>
                        <p>Email me at adrianna.l.jones1@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AboutPage
