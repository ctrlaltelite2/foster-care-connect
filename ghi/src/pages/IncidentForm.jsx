import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useCreateIncidentsMutation } from '../features/Slices/IncidentSlice'
import { useGetChildByIdQuery } from '../features/Slices/ChildSlice'
import { useGetTokenQuery } from '../features/auth/authSlice'
import '../css/IncidentForm.css'

const IncidentReportForm = () => {
    const { id } = useParams()
    const childId = parseInt(id)
    const navigate = useNavigate()
    const [createIncident, { isLoading, isError }] =
        useCreateIncidentsMutation()
    const [isChecked, setIsChecked] = useState(false)
    const { data, isLoading: tokenIsLoading } = useGetTokenQuery()

    const {
        data: children,
        isError: childrenError,
        isLoading: childrenLoading,
    } = useGetChildByIdQuery(childId)

    useEffect(() => {
        if (tokenIsLoading) {
            return
        }

        if (!data) {
            navigate(`/child/profile/${childId}`)
        }
    }, [data, navigate, tokenIsLoading, childId])

    const [incidentData, setIncidentData] = useState({
        children_id: childId,
        name: '',
        picture: '',
        description: '',
        police_involved: false,
        action_plan: '',
    })

    const handleChange = (e) => {
        setIncidentData({ ...incidentData, [e.target.name]: e.target.value })
    }

    const handleCheckedChange = (e) => {
        setIsChecked(e.target.checked)
        setIncidentData({ ...incidentData, police_involved: e.target.checked })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setIncidentData({ ...incidentData, children_id: childId })
        try {
            await createIncident(incidentData).unwrap()
            setIncidentData({
                name: '',
                picture: '',
                description: '',
                police_involved: false,
                action_plan: '',
            })
            navigate(`/child/profile/${childId}`)
        } catch (error) {
            console.error('Failed to create report:', error)
            alert('Something Went Wrong')
        }
    }

    if (childrenLoading) {
        return <div>Loading...</div>
    }
    if (childrenError || !children) {
        return <div>Error loading children</div>
    }

    return (
        <>
            <div className="addIncidentForm">
                <h2 className="incidentFormTitle">
                    Add an Incident Report for {children.name}
                </h2>
                <form className="incidentForm" onSubmit={handleSubmit}>
                    <div>
                        <div className="incidentInfoForm">
                            <div className="incidentFormInput">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    value={incidentData.name}
                                    onChange={handleChange}
                                />
                            </div>
                            <div className="incidentFormInput">
                                <label htmlFor="description">Description</label>
                                <input
                                    type="text"
                                    name="description"
                                    id="description"
                                    value={incidentData.description}
                                    onChange={handleChange}
                                />
                            </div>

                            <div className="incidentFormInput">
                                <label htmlFor="picture">Upload Image</label>
                                <input
                                    type="text"
                                    name="picture"
                                    id="picture"
                                    value={incidentData.picture}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className="incidentFormInput incidentFormTextarea">
                            <label htmlFor="action_plan">Action Plan</label>
                            <textarea
                                id="action_plan"
                                name="action_plan"
                                value={incidentData.action_plan}
                                onChange={handleChange}
                            ></textarea>
                        </div>
                        <div className="incidentFormCheck">
                            <label htmlFor="police_involved">
                                <p>
                                    Police Involved? {isChecked ? 'Yes' : 'No'}.
                                </p>
                            </label>
                            <input
                                type="checkbox"
                                name="police_involved"
                                checked={isChecked}
                                id="police_involved"
                                value={incidentData.police_involved}
                                onChange={handleCheckedChange}
                            />
                        </div>
                    </div>
                    <button
                        type="submit"
                        className="incidentFormBtn"
                        disabled={isLoading}
                    >
                        Submit
                    </button>
                </form>
                {isError && <p>Something has gone wrong</p>}
            </div>
        </>
    )
}

export default IncidentReportForm
