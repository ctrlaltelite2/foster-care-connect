import { useParams, useNavigate, Link } from 'react-router-dom'
import { useGetParentByIdQuery } from '../features/Slices/ParentSlice'
import { useGetAllChildrenQuery } from '../features/Slices/ChildSlice'
import { useGetAllAgentsQuery } from '../features/Slices/AgentSlice'
import { useGetTokenQuery } from '../features/auth/authSlice'
import '../css/ParentProfile.css'
import defaultProfile from '../assets/defaultProfile.jpg'
import ChildCard from '../components/ChildCard'

const ParentProfile = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const {
        data: parent,
        isError: parentError,
        isLoading: parentLoading,
    } = useGetParentByIdQuery(id)
    const { data: tokenData } = useGetTokenQuery()
    const {
        data: agents,
        isLoading: agentsLoading,
    } = useGetAllAgentsQuery()
    const {
        data: children,
        isError: childrenError,
        isLoading: childrenLoading,
    } = useGetAllChildrenQuery()

    if (parentLoading || childrenLoading || agentsLoading) {
        return <div>Loading...</div>
    }

    if (parentError || !parent) {
        return <div>Error loading profile.</div>
    }

    if (childrenError || !children) {
        return <div>Error loading children</div>
    }

    const filteredChildren = children.filter(
        (child) => child.parents_id === parseInt(id)
    )
    const totalCompensation = filteredChildren.length * 800
    if (parentLoading || childrenLoading) return <div>Loading...</div>
    if (parentError || !parent) return <div>Error loading profile.</div>
    if (childrenError) return <div>Error loading children</div>

    if (tokenData.account.id !== parent.account_id) {
        navigate(`/`)
        return null
    }

    const handleUpdate = () => {
        navigate(`/parent/editprofile/${id}`)
    }

    return (
        <div className="parentProfile">
            <h1 className="parentProfileWelcome">
                Welcome {parent.parent1_name} & {parent.parent2_name}
            </h1>
            <div className="parentProfileInfo">
                <div className="parentProfileImgInfo">
                    <div className="parentProfileImgUpdate">
                        <img
                            src={
                                parent.profile_picture !== ''
                                    ? parent.profile_picture
                                    : defaultProfile
                            }
                            alt={`${parent.parent1_name} and ${parent.parent2_name}`}
                            className="parentProfilePicture"
                        />

                        <button
                            className="parentProfileUpdateBtn"
                            onClick={handleUpdate}
                        >
                            Update Profile
                        </button>

                        <p className="parentProfileTotalComp">
                            Total Compensation: ${totalCompensation}.00
                        </p>
                    </div>
                    <div className="parentProfileDetails">
                        <p>Parent: {parent.parent1_name}</p>
                        <p>Parent: {parent.parent2_name}</p>
                        <p>Location: {parent.location}</p>
                        <p>
                            Current number of Children:{' '}
                            {filteredChildren.length}
                        </p>
                    </div>
                </div>
                <div className="parentProfileContact">
                    <p>(542)582-3256</p>
                    <p>(542)582-6523</p>
                    <p className="parentProfileEmail">Parent@parent.com</p>
                </div>
            </div>
            <div className="children-of-parent">
                <ul className="parentProfileChildrenGrid">
                    {agents &&
                        filteredChildren.map((child) => (
                            <li className='parent-child-card' key={child.id}>
                                <Link to={`/child/profile/${child.id}`}>
                                    <ChildCard child={child} agents={agents} />
                                </Link>
                            </li>
                        ))}
                </ul>
            </div>
        </div>
    )
}

export default ParentProfile
