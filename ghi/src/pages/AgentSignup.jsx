import { useState, useEffect, useCallback } from 'react'
import { useSignupMutation } from '../features/auth/authSlice'
import { useCreateAgentMutation } from '../features/Slices/AgentSlice'
import { useNavigate } from 'react-router-dom'
import '../css/AgentEditProfile.css'

const AgentSignup = () => {
    const [signup, { isSuccess, isError, error, data }] = useSignupMutation()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState('')
    const navigate = useNavigate()

    const [createAgent, { isLoading: isAgentLoading }] =
        useCreateAgentMutation()

    const [agentData, setAgentData] = useState({
        profile_picture: '',
        name: '',
        employee_id: '',
        account_id: '',
    })

    const createAgentProfile = useCallback(
        async (accountId) => {
            try {
                const agentProfileData = { ...agentData, account_id: accountId }
                await createAgent(agentProfileData).unwrap()
                alert('Account and Profile created Successfully!')
                navigate('/agent-login')
            } catch (error) {
                console.error('Failed to create agent:', error)
            }
        },
        [createAgent, navigate, agentData]
    )

    const createAgentProfileCallback = useCallback(
        () => createAgentProfile(data?.account?.id),
        [data?.account?.id, createAgentProfile]
    )

    useEffect(() => {
        if (isSuccess) {
            const accountId = data?.account?.id
            if (accountId) {
                createAgentProfileCallback(accountId)
            }
        } else if (isError) {
            setErrorMessage(
                error?.data?.detail || 'An unexpected signup error occurred'
            )
        }
    }, [isSuccess, isError, error, data, createAgentProfileCallback])

    const handleSubmit = (e) => {
        e.preventDefault()
        if (password !== confirmPassword) {
            setErrorMessage('Passwords do not match')
            return
        }
        signup({ username, password })
    }

    const handleAgentDataChange = (e) => {
        setAgentData({ ...agentData, [e.target.name]: e.target.value })
    }

    return (
        <div className="agentEditPage">
            <div className="col-md-6 offset-md-3">
                <h1 className="agentEditPageTitle">Add A Agent</h1>
                {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                        {errorMessage}
                    </div>
                )}
                <form className="agentEditForm" onSubmit={handleSubmit}>
                    <div>
                        <p className="parentSignupPageSubitle">
                            Account Information
                        </p>
                        <div className="agentEditInfoForm">
                            <div className="agentEditInput">
                                <label
                                    htmlFor="Signup__username"
                                    className="form-label"
                                >
                                    Username
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="Signup__username"
                                    value={username}
                                    onChange={(e) =>
                                        setUsername(e.target.value)
                                    }
                                />
                            </div>
                            <div className="agentEditInput">
                                <label
                                    htmlFor="Signup__password"
                                    className="form-label"
                                >
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="Signup__password"
                                    value={password}
                                    onChange={(e) =>
                                        setPassword(e.target.value)
                                    }
                                />
                            </div>
                            <div className="agentEditInput">
                                <label
                                    htmlFor="Signup__confirmPassword"
                                    className="form-label"
                                >
                                    Confirm Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="Signup__confirmPassword"
                                    value={confirmPassword}
                                    onChange={(e) =>
                                        setConfirmPassword(e.target.value)
                                    }
                                />
                            </div>
                        </div>
                        <div>
                            <p className="parentSignupPageSubitle">
                                Agent Information
                            </p>
                            <div className="agentEditInfoForm">
                                <div className="agentEditInput">
                                    <label>Name</label>
                                    <input
                                        type="text"
                                        name="name"
                                        value={agentData.name}
                                        onChange={handleAgentDataChange}
                                    />
                                </div>
                                <div className="agentEditInput">
                                    <label>Picture</label>
                                    <input
                                        type="text"
                                        name="profile_picture"
                                        value={agentData.profile_picture}
                                        onChange={handleAgentDataChange}
                                    />
                                </div>
                                <div className="agentEditInput">
                                    <label>Employee ID</label>
                                    <input
                                        type="text"
                                        name="employee_id"
                                        value={agentData.employee_id}
                                        onChange={handleAgentDataChange}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <button
                        type="submit"
                        className="agentEditBtn"
                        disabled={isAgentLoading}
                    >
                        Sign Up
                    </button>
                </form>
            </div>
        </div>
    )
}

export default AgentSignup
