import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetAgentByIdQuery,
    useUpdateAgentMutation,
    useDeleteAgentMutation,
} from '../features/Slices/AgentSlice'
import {
    useDeleteMutation,
    useLogoutMutation,
} from '../features/auth/authSlice'
import '../css/AgentEditProfile.css'

const EditAgentProfile = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const { data: agentData, isLoading, isError } = useGetAgentByIdQuery(id)
    const [updateAgent, { isLoading: isUpdating }] = useUpdateAgentMutation(id)
    const [deleteAgent] = useDeleteAgentMutation(id)
    const [deleteAccount] = useDeleteMutation(id)
    const [logout] = useLogoutMutation()
    const [errorMessage, setErrorMessage] = useState('')

    const [formData, setFormData] = useState({
        id: id,
        profile_picture: '',
        name: '',
        employee_id: '',
        account_id: agentData.account_id,
    })

    useEffect(() => {
        if (agentData) {
            setFormData({
                id: id,
                profile_picture: agentData.profile_picture,
                name: agentData.name,
                employee_id: agentData.employee_id,
                account_id: agentData.account_id,
            })
        }
    }, [agentData, id])

    const handleInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await updateAgent({ agentId: id, ...formData }).unwrap()
            navigate(`/agent/profile/${id}`)
        } catch (error) {
            setErrorMessage('Failed to update profile')
        }
    }
    const handleDelete = async () => {
        try {
            await deleteAgent(id).unwrap()
            await deleteAccount(agentData.account_id).unwrap()
            logout()
            navigate('/')
        } catch (error) {
            setErrorMessage(
                'Failed to delete profile you still have Foster Children'
            )
        }
    }
    if (isLoading) return <div>Loading...</div>
    if (isError) return <div>Error loading profile data</div>

    return (
        <div className="agentEditPage">
            <h1 className="agentEditPageTitle">Edit Agent Profile</h1>

            <form className="agentEditForm" onSubmit={handleSubmit}>
                <div className="agentEditInfo">
                    <p className="agentEditPageSubitle">Agent Information</p>
                    <div className="agentEditInfoForm">
                        <div className="agentEditInput">
                            <label>Profile Picture URL:</label>
                            <input
                                type="text"
                                name="profile_picture"
                                id="profile_picture"
                                value={formData.profile_picture}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="agentEditInput">
                            <label>Name:</label>
                            <input
                                type="text"
                                name="name"
                                id="name"
                                value={formData.name}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="agentEditInput">
                            <label>Employee ID:</label>
                            <input
                                type="text"
                                name="employee_id"
                                id="employee_id"
                                value={formData.employee_id}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="agentEditBtn"
                    disabled={isUpdating}
                >
                    Update Profile
                </button>
            </form>
            <div className="agentDeleteBtn">
                <button className="agentDeleteBtn1" onClick={handleDelete}>
                    Delete Profile
                </button>
                {errorMessage && (
                    <div className="agentError">{errorMessage}</div>
                )}
            </div>
        </div>
    )
}

export default EditAgentProfile
