import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetParentByIdQuery,
    useUpdateParentMutation,
    useDeleteParentMutation,
} from '../features/Slices/ParentSlice'
import {
    useDeleteMutation,
    useLogoutMutation,
} from '../features/auth/authSlice'
import '../css/ParentEditProfile.css'

const EditParentProfile = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const { data: parentData, isLoading, isError } = useGetParentByIdQuery(id)
    const [updateParent, { isLoading: isUpdating }] =
        useUpdateParentMutation(id)
    const [deleteParent] = useDeleteParentMutation(id)
    const [deleteAccount] = useDeleteMutation(id)
    const [logout] = useLogoutMutation()
    const [formData, setFormData] = useState({
        id: id,
        parent1_name: '',
        parent2_name: '',
        location: '',
        account_id: parentData.account_id,
        profile_picture: '',
    })
    const [errorMessage, setErrorMessage] = useState('')

    useEffect(() => {
        if (parentData) {
            setFormData({
                id: id,
                parent1_name: parentData.parent1_name,
                parent2_name: parentData.parent2_name,
                location: parentData.location,
                account_id: parentData.account_id,
                profile_picture: parentData.profile_picture,
            })
        }
    }, [parentData, id])

    const handleInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await updateParent({ parentId: id, ...formData }).unwrap()
            navigate(`/parent/profile/${id}`)
        } catch (error) {
            setErrorMessage('Failed to update profile')
        }
    }
    const handleDelete = async () => {
        try {
            await deleteParent(id).unwrap()
            await deleteAccount(parentData.account_id).unwrap()
            logout()
            navigate('/')
        } catch (error) {
            setErrorMessage(
                'Failed to delete profile you still have Foster Children'
            )
        }
    }

    if (isLoading) return <div>Loading...</div>
    if (isError) return <div>Error loading profile data</div>

    return (
        <div className="parentEditPage">
            <h1 className="parentEditPageTitle">Edit Parent Profile</h1>
            <form className="parentEditForm" onSubmit={handleSubmit}>
                <div className="parentEditFamilyInfo">
                    <p className="parentEditPageSubitle">Family Information</p>
                    <div className="parentEditFamilyInfoForm">
                        <div className="parentEditInput">
                            <label>Parent 1 Name:</label>
                            <input
                                type="text"
                                name="parent1_name"
                                value={formData.parent1_name}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="parentEditInput">
                            <label>Parent 2 Name:</label>
                            <input
                                type="text"
                                name="parent2_name"
                                value={formData.parent2_name}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="parentEditInput">
                            <label>Location:</label>
                            <input
                                type="text"
                                name="location"
                                value={formData.location}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="parentEditInput">
                            <label>Profile Picture URL:</label>
                            <input
                                type="text"
                                name="profile_picture"
                                value={formData.profile_picture}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                </div>
                <button
                    type="submit"
                    className="parentEditBtn"
                    disabled={isUpdating}
                >
                    Update Profile
                </button>
            </form>
            <div className="parentDeleteBtn">
                <button className="parentDeleteBtn1" onClick={handleDelete}>
                    Delete Profile
                </button>
                {errorMessage && (
                    <div className="parentError">{errorMessage}</div>
                )}
            </div>
        </div>
    )
}

export default EditParentProfile
