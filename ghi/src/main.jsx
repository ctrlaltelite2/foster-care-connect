import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import { Provider } from 'react-redux'
import App from './App'
import HomePage from './pages/HomePage'
import AgentLogin from './pages/AgentLogIn'
import AgentSignup from './pages/AgentSignup'
import ParentLogin from './pages/ParentLogin'
import { store } from './store'
import ParentSignup from './pages/ParentsSignup'
import MedicationForm from './pages/MedicationForm'
import ChildSignupPage from './pages/ChildSignup'
import ParentProfile from './pages/ParentProfile'
import MedApptForm from './pages/MedicalApptForm'
import IncidentReportForm from './pages/IncidentForm'
import ChildProfile from './pages/ChildProfile'
import ToyForm from './pages/ToyForm'
import AgentProfile from './pages/AgentProfile'
import EditParentProfile from './pages/ParentEditProfile'
import IncidentsEditForm from './pages/IncidentEditForm'
import EditAgentProfile from './pages/AgentEditProfile'
import EditChildProfile from './pages/ChildEditProfile'
import MedApptEditForm from './pages/MedicalApptEdit'
import EditToyForm from './pages/ToyEdit'
import EditMedicationForm from './pages/MedicationEdit'
import AboutPage from './pages/AboutPage'

const router = createBrowserRouter([
    {
        element: <App />,
        basename: '/foster-care-connect',
        children: [
            {
                path: '/',
                element: <HomePage />,
            },
            {
                path: '/about',
                element: <AboutPage />,
            },
            {
                path: '/parent-login',
                element: <ParentLogin />,
            },
            {
                path: '/agent-login',
                element: <AgentLogin />,
            },
            {
                path: '/agent-signup',
                element: <AgentSignup />,
            },
            {
                path: '/agent/profile/:id',
                element: <AgentProfile />,
            },
            {
                path: '/agent/editprofile/:id',
                element: <EditAgentProfile />,
            },
            {
                path: '/parent-signup',
                element: <ParentSignup />,
            },
            {
                path: '/child/profile/:id/create-medication',
                element: <MedicationForm />,
            },
            {
                path: '/child-signup',
                element: <ChildSignupPage />,
            },
            {
                path: '/parent/profile/:id',
                element: <ParentProfile />,
            },
            {
                path: '/parent/editprofile/:id',
                element: <EditParentProfile />,
            },
            {
                path: '/child/profile/:id/create-medappt',
                element: <MedApptForm />,
            },
            {
                path: '/child/profile/:id/create-incident',
                element: <IncidentReportForm />,
            },
            {
                path: '/child/profile/:id',
                element: <ChildProfile />,
            },
            {
                path: '/child/editprofile/:id',
                element: <EditChildProfile />,
            },
            {
                path: '/child/profile/:id/create-toy',
                element: <ToyForm />,
            },
            {
                path: '/incidents/edit/:id',
                element: <IncidentsEditForm />,
            },
            {
                path: '/medappt/edit/:id',
                element: <MedApptEditForm />,
            },
            {
                path: '/toy/edit/:id',
                element: <EditToyForm />,
            },
            {
                path: '/medication/edit/:id',
                element: <EditMedicationForm />,
            },
        ],
    },
])
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router} />
        </Provider>
    </React.StrictMode>
)
