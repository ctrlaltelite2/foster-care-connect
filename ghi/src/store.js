import { configureStore } from '@reduxjs/toolkit'
import { authApi } from './features/auth/authSlice'
import { childApi } from './features/Slices/ChildSlice'
import { medApptApi } from './features/Slices/medApptSlice'
import { parentApi } from './features/Slices/ParentSlice'
import { incidentsApi } from './features/Slices/IncidentSlice'
import { medicationApi } from './features/Slices/MedicationSlice'
import { toyApi } from './features/Slices/ToySlice'
import { AgentApi } from './features/Slices/AgentSlice'

export const store = configureStore({
    reducer: {
        [authApi.reducerPath]: authApi.reducer,
        [childApi.reducerPath]: childApi.reducer,
        [parentApi.reducerPath]: parentApi.reducer,
        [medApptApi.reducerPath]: medApptApi.reducer,
        [incidentsApi.reducerPath]: incidentsApi.reducer,
        [medicationApi.reducerPath]: medicationApi.reducer,
        [toyApi.reducerPath]: toyApi.reducer,
        [AgentApi.reducerPath]: AgentApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(
            authApi.middleware,
            childApi.middleware,
            parentApi.middleware,
            medApptApi.middleware,
            incidentsApi.middleware,
            medicationApi.middleware,
            toyApi.middleware,
            AgentApi.middleware
        ),
})
