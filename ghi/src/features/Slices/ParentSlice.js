import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const parentApi = createApi({
    reducerPath: 'parentApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    tagTypes: ['Parent'],
    endpoints: (builder) => ({
        createParent: builder.mutation({
            query: (parentData) => ({
                url: '/api/parents',
                method: 'POST',
                body: parentData,
            }),
            invalidatesTags: ['Parent'],
        }),
        getParentById: builder.query({
            query: (parentId) => ({
                url: `/api/parents/${parentId}`,
            }),
            providesTags: ['Parent'],
        }),
        updateParent: builder.mutation({
            query: ({ parentId, ...parentData }) => ({
                url: `/api/parents/${parentId}`,
                method: 'PUT',
                body: parentData,
            }),
            invalidatesTags: ['Parent'],
        }),
        deleteParent: builder.mutation({
            query: (parentId) => ({
                url: `/api/parents/${parentId}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Parent'],
        }),
        getAllParents: builder.query({
            query: () => ({
                url: '/api/parents',
            }),
            providesTags: ['Parent'],
        }),
    }),
})

export const {
    useCreateParentMutation,
    useGetParentByIdQuery,
    useUpdateParentMutation,
    useDeleteParentMutation,
    useGetAllParentsQuery,
} = parentApi

export default parentApi
