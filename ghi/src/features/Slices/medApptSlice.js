import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const medApptApi = createApi({
    reducerPath: 'med_appts',
    tagTypes: ['MedAppt'],
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    endpoints: (builder) => ({
        getMedAppts: builder.query({
            query: () => ({
                url: '/api/med_appts',
            }),
            providesTags: ['MedAppt'],
        }),
        getMedApptById: builder.query({
            query: (children_id) => ({
                url: `/api/med_appts/${children_id}`,
            }),
            providesTags: ['MedAppt'],
        }),
        createMedAppts: builder.mutation({
            query: (apptData) => ({
                url: '/api/med_appts',
                method: 'POST',
                body: apptData,
            }),
            invalidatesTags: ['MedAppt'],
        }),
        updateMedAppts: builder.mutation({
            query: ({ apptid, apptData }) => ({
                url: `/api/med_appts/${apptid}`,
                method: 'PUT',
                body: apptData,
            }),
            invalidatesTags: ['MedAppt'],
        }),
        deleteMedAppts: builder.mutation({
            query: (children_id) => ({
                url: `/api/med_appts/${children_id}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['MedAppt'],
        }),
    }),
})

export const {
    useGetMedApptsQuery,
    useGetMedApptByIdQuery,
    useCreateMedApptsMutation,
    useUpdateMedApptsMutation,
    useDeleteMedApptsMutation,
} = medApptApi

export default medApptApi
