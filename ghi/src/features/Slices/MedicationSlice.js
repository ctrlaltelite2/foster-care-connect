import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const medicationApi = createApi({
    reducerPath: 'medicationApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    tagTypes: ['Medication'],
    endpoints: (builder) => ({
        createMedication: builder.mutation({
            query: (medicationData) => ({
                url: '/api/medications',
                method: 'POST',
                body: medicationData,
            }),
            invalidatesTags: ['Medication'],
        }),
        getMedicationById: builder.query({
            query: (medicationId) => ({
                url: `/api/medications/${medicationId}`,
            }),
            providesTags: ['Medication'],
        }),
        updateMedication: builder.mutation({
            query: ({ medicationId, ...medicationData }) => ({
                url: `/api/medications/${medicationId}`,
                method: 'PUT',
                body: medicationData,
            }),
            invalidatesTags: ['Medication'],
        }),
        deleteMedication: builder.mutation({
            query: (medicationId) => ({
                url: `/api/medications/${medicationId}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Medication'],
        }),
        getAllMedications: builder.query({
            query: () => ({
                url: '/api/medications',
            }),
            providesTags: ['Medication'],
        }),
    }),
})

export const {
    useCreateMedicationMutation,
    useGetMedicationByIdQuery,
    useUpdateMedicationMutation,
    useDeleteMedicationMutation,
    useGetAllMedicationsQuery,
} = medicationApi

export default medicationApi
