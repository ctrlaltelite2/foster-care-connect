import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const incidentsApi = createApi({
    reducerPath: 'incidents',
    tagTypes: ['Incidents'],
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    endpoints: (builder) => ({
        getIncidents: builder.query({
            query: () => ({
                url: '/api/incidents',
            }),
            providesTags: ['Incidents'],
        }),
        getIncidentsById: builder.query({
            query: (IncidentsId) => ({
                url: `/api/incidents/${IncidentsId}`,
            }),
            providesTags: ['Incidents'],
        }),
        createIncidents: builder.mutation({
            query: (incidentData) => ({
                url: '/api/incidents',
                method: 'POST',
                body: incidentData,
            }),
            invalidatesTags: ['Incidents'],
        }),
        updateIncidents: builder.mutation({
            query: ({ IncidentId, ...incidentData }) => ({
                url: `/api/incidents/${IncidentId}`,
                method: 'PUT',
                body: incidentData,
            }),
            invalidatesTags: ['Incidents'],
        }),
        deleteIncidents: builder.mutation({
            query: (children_id) => ({
                url: `/api/incidents/${children_id}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Incidents'],
        }),
    }),
})

export const {
    useGetIncidentsQuery,
    useGetIncidentsByIdQuery,
    useCreateIncidentsMutation,
    useDeleteIncidentsMutation,
    useUpdateIncidentsMutation,
} = incidentsApi

export default incidentsApi
