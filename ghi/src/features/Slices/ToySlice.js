import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const toyApi = createApi({
    reducerPath: 'toyApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    tagTypes: ['Toy'],
    endpoints: (builder) => ({
        createToy: builder.mutation({
            query: (toyData) => ({
                url: '/api/toys',
                method: 'POST',
                body: toyData,
            }),
            invalidatesTags: ['Toy'],
        }),
        getToyById: builder.query({
            query: (toyId) => ({
                url: `/api/toys/${toyId}`,
            }),
            providesTags: ['Toy'],
        }),
        updateToy: builder.mutation({
            query: ({ toyId, ...toyData }) => ({
                url: `/api/toys/${toyId}`,
                method: 'PUT',
                body: toyData,
            }),
            invalidatesTags: ['Toy'],
        }),
        deleteToy: builder.mutation({
            query: (toyId) => ({
                url: `/api/toys/${toyId}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Toy'],
        }),
        getAllToys: builder.query({
            query: () => ({
                url: '/api/toys',
            }),
            providesTags: ['Toy'],
        }),
    }),
})

export const {
    useCreateToyMutation,
    useGetToyByIdQuery,
    useUpdateToyMutation,
    useDeleteToyMutation,
    useGetAllToysQuery,
} = toyApi

export default toyApi
