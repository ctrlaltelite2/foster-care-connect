import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const childApi = createApi({
    reducerPath: 'childApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    tagTypes: ['Child'],
    endpoints: (builder) => ({
        createChild: builder.mutation({
            query: (childData) => ({
                url: '/api/children',
                method: 'POST',
                body: childData,
            }),
            invalidatesTags: ['Child'],
        }),
        getChildById: builder.query({
            query: (childId) => ({
                url: `/api/children/${childId}`,
            }),
            providesTags: ['Child'],
        }),
        updateChild: builder.mutation({
            query: ({ childId, ...childData }) => ({
                url: `/api/children/${childId}`,
                method: 'PUT',
                body: childData,
            }),
            invalidatesTags: ['Child'],
        }),
        deleteChild: builder.mutation({
            query: (childId) => ({
                url: `/api/children/${childId}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Child'],
        }),
        getAllChildren: builder.query({
            query: () => ({
                url: '/api/children',
            }),
            providesTags: ['Child'],
        }),
    }),
})

export const {
    useCreateChildMutation,
    useGetChildByIdQuery,
    useUpdateChildMutation,
    useDeleteChildMutation,
    useGetAllChildrenQuery,
} = childApi

export default childApi
