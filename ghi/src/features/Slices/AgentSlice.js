import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const AgentApi = createApi({
    reducerPath: 'agentApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    tagTypes: ['Agent'],
    endpoints: (builder) => ({
        createAgent: builder.mutation({
            query: (agentData) => ({
                url: '/api/agents',
                method: 'POST',
                body: agentData,
            }),
            invalidatesTags: ['Agent'],
        }),
        getAgentById: builder.query({
            query: (agentId) => ({
                url: `/api/agents/${agentId}`,
            }),
            providesTags: ['Agent'],
        }),
        updateAgent: builder.mutation({
            query: ({ agentId, ...agentData }) => ({
                url: `/api/agents/${agentId}`,
                method: 'PUT',
                body: agentData,
            }),
            invalidatesTags: ['Agent'],
        }),
        deleteAgent: builder.mutation({
            query: (agentId) => ({
                url: `/api/agents/${agentId}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Agent'],
        }),
        getAllAgents: builder.query({
            query: () => ({
                url: '/api/agents',
            }),
            providesTags: ['Agent'],
        }),
    }),
})

export const {
    useCreateAgentMutation,
    useGetAgentByIdQuery,
    useUpdateAgentMutation,
    useDeleteAgentMutation,
    useGetAllAgentsQuery,
} = AgentApi

export default AgentApi
