import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const authApi = createApi({
    reducerPath: 'authentication',
    tagTypes: ['Account'],
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_APP_API_HOST,
    }),
    endpoints: (builder) => ({
        login: builder.mutation({
            query: (info) => {
                const formData = new FormData()
                formData.append('username', info.username)
                formData.append('password', info.password)

                return {
                    url: '/token',
                    method: 'POST',
                    body: formData,
                    credentials: 'include',
                }
            },
            invalidatesTags: ['Account'],
        }),
        getToken: builder.query({
            query: () => ({
                url: '/token',
                credentials: 'include',
            }),
            providesTags: ['Account'],
        }),
        signup: builder.mutation({
            query: (credentials) => ({
                url: '/api/account',
                method: 'POST',
                body: {
                    username: credentials.username,
                    password: credentials.password,
                },
            }),
            invalidatesTags: ['Account'],
        }),
        delete: builder.mutation({
            query: (accountId) => ({
                url: `/api/account/${accountId}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Account'],
        }),
        update: builder.mutation({
            query: ({ accountId, ...accountData }) => ({
                url: `/api/account/${accountId}`,
                method: 'PUT',
                body: accountData,
            }),
            invalidatesTags: ['Account'],
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['Account'],
        }),
    }),
})

export const {
    useLoginMutation,
    useGetTokenQuery,
    useSignupMutation,
    useDeleteMutation,
    useUpdateMutation,
    useLogoutMutation,
} = authApi

export default authApi
