import { useDeleteMedApptsMutation } from '../../features/Slices/medApptSlice'
import trash from '../../assets/trash.png'
import edit from '../../assets/edit.png'
import { Link } from 'react-router-dom'
import '../../css/MedicalApptList.css'

const MedApptsList = ({ childId, medappt }) => {
    const filteredMedAppts = medappt.filter(
        (medAppts) => medAppts.children_id === parseInt(childId)
    )
    const [deleteMedAppts] = useDeleteMedApptsMutation()
    const handleDeleteAppt = async (id) => {
        deleteMedAppts(id)
    }

    return (
        <div className="medicalApptList">
            <h2 className="medicalApptListTitle">Medical Appointments</h2>
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Location</th>
                        <th>Reason</th>
                        <th>Description of Care</th>
                        <th>Treatment Plan</th>
                        <th>Child ID</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredMedAppts?.map((appt) => (
                        <tr key={appt.id}>
                            <td>{appt.date}</td>
                            <td>{appt.location}</td>
                            <td>{appt.reason}</td>
                            <td>{appt.description_of_care}</td>
                            <td>{appt.treatment_plan}</td>
                            <td>{appt.children_id}</td>
                            <td>
                                <button className="incidentListEdit">
                                    <Link to={`/medappt/edit/${appt.id}`}>
                                        <img
                                            className="incidentListEdit"
                                            src={edit}
                                        />
                                    </Link>
                                </button>
                                <button
                                    onClick={() => handleDeleteAppt(appt.id)}
                                    className="incidentListDelete"
                                >
                                    <img
                                        className="incidentListEdit"
                                        src={trash}
                                    />
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default MedApptsList
