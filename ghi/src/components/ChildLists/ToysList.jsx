import { Link } from 'react-router-dom'
import { useDeleteToyMutation } from '../../features/Slices/ToySlice'
import trash from '../../assets/trash.png'
import edit from '../../assets/edit.png'
import '../../css/ToysList.css'

const ToysList = ({ childId, toy }) => {
    const filteredToys = toy.filter(
        (toys) => toys.children_id === parseInt(childId)
    )
    const [deleteToy] = useDeleteToyMutation()
    const handleDeleteToy = async (id) => {
        deleteToy(id)
    }

    return (
        <div className="toyList">
            <h2 className="toyListTitle">Toys List</h2>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Picture</th>
                        <th>Description</th>
                        <th>Condition</th>
                        <th>Assigned to Child ID</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredToys?.map((toy) => (
                        <tr key={toy.id}>
                            <td>{toy.name}</td>
                            <td>
                                <img
                                    src={toy.picture}
                                    alt={`Picture of ${toy.name}`}
                                    className="toyListImg"
                                />
                            </td>
                            <td>{toy.description}</td>
                            <td>{toy.condition}</td>
                            <td>{toy.children_id}</td>
                            <td>
                                <button className="incidentListEdit">
                                    <Link to={`/toy/edit/${toy.id}`}>
                                        <img
                                            className="incidentListEdit"
                                            src={edit}
                                        />
                                    </Link>
                                </button>
                                <button
                                    onClick={() => handleDeleteToy(toy.id)}
                                    className="incidentListDelete"
                                >
                                    <img
                                        className="incidentListEdit"
                                        src={trash}
                                    />
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default ToysList
