import { useDeleteIncidentsMutation } from '../../features/Slices/IncidentSlice'
import { Link } from 'react-router-dom'
import trash from '../../assets/trash.png'
import edit from '../../assets/edit.png'
import '../../css/IncidentsList.css'

const IncidentsList = ({ childId, incident }) => {
    const filteredIncidents = incident.filter(
        (incidents) => incidents.children_id === parseInt(childId)
    )
    const [deleteIncident] = useDeleteIncidentsMutation()
    const handleDeleteIncident = async (id) => {
        deleteIncident(id)
    }

    return (
        <div className="incidentList">
            <h2 className="incidentListTitle">Incidents</h2>
            <table>
                <thead>
                    <tr>
                        <th>Incident</th>
                        <th>Picture</th>
                        <th>Description</th>
                        <th>Police Involvment</th>
                        <th>Action Plan</th>
                        <th>Child ID</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredIncidents?.map((incident) => (
                        <tr key={incident.id}>
                            <td>{incident.name}</td>
                            <td>
                                <img
                                    className="incidentListImg"
                                    src={incident.picture}
                                    alt={`Picture of ${incident.name}`}
                                />
                            </td>
                            <td>{incident.description}</td>
                            <td>{incident.police_involved ? 'Yes' : 'No'}</td>
                            <td>{incident.action_plan}</td>
                            <td>{incident.children_id}</td>
                            <td>
                                <button className="incidentListEdit">
                                    <Link to={`/incidents/edit/${incident.id}`}>
                                        <img
                                            className="incidentListEdit"
                                            src={edit}
                                        />
                                    </Link>
                                </button>
                                <button
                                    onClick={() =>
                                        handleDeleteIncident(incident.id)
                                    }
                                    className="incidentListDelete"
                                >
                                    <img
                                        className="incidentListEdit"
                                        src={trash}
                                    />
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default IncidentsList
