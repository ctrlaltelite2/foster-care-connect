import { useDeleteMedicationMutation } from '../../features/Slices/MedicationSlice'
import { Link } from 'react-router-dom'
import trash from '../../assets/trash.png'
import edit from '../../assets/edit.png'
import '../../css/MedicationsList.css'

const MedicationList = ({ childId, medication }) => {
    const filteredMeds = medication.filter(
        (medications) => medications.children_id === parseInt(childId)
    )

    const [deleteMedication] = useDeleteMedicationMutation()
    const handleDeleteMed = async (id) => {
        deleteMedication(id)
    }

    return (
        <div className="medicationList">
            <h1 className="medicationListTitle">Medication List</h1>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Dosage</th>
                        <th>Quantity</th>
                        <th>Expiration Date</th>
                        <th>Child&apos;s ID</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredMeds.map((medication) => (
                        <tr key={medication.id}>
                            <td>{medication.name}</td>
                            <td>{medication.dosage}</td>
                            <td>{medication.quantity}</td>
                            <td>{medication.expiration_date}</td>
                            <td>{medication.children_id}</td>
                            <td>
                                <button className="incidentListEdit">
                                    <Link
                                        to={`/medication/edit/${medication.id}`}
                                    >
                                        <img
                                            className="incidentListEdit"
                                            src={edit}
                                        />
                                    </Link>
                                </button>
                                <button
                                    onClick={() =>
                                        handleDeleteMed(medication.id)
                                    }
                                    className="incidentListDelete"
                                >
                                    <img
                                        className="incidentListEdit"
                                        src={trash}
                                    />
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default MedicationList
