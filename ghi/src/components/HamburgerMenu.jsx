import { useState } from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { useGetTokenQuery, useLogoutMutation } from '../features/auth/authSlice'
import '../css/HamBurgerMenu.css'

const HamburgerMenu = () => {
    const [isOpen, setIsOpen] = useState(false)
    const navigate = useNavigate()
    const { data: account } = useGetTokenQuery()
    const [logout] = useLogoutMutation()
    const toggleMenu = () => {
        setIsOpen(!isOpen)
    }

    const closeMenu = () => {
        setIsOpen(false)
    }

    const handleLogout = async () => {
        try {
            await logout()
                .unwrap()
                .then(() => {
                    navigate('/')
                    closeMenu()
                })
                .catch((error) => {
                    console.error('Logout failed', error)
                })
        } catch (error) {
            console.error('Error during logout:', error)
        }
    }

    return (
        <div className="hamburger-menu">
            <div
                className={`menu-icon ${isOpen ? 'open' : ''}`}
                onClick={toggleMenu}
            >
                <div className="bar"></div>
                <div className="bar"></div>
                <div className="bar"></div>
            </div>

            {isOpen && (
                <div className="openMenu">
                    <div className="menu-items">
                        <div className="menu-1 menuMargin">
                            <Link to={'/'} onClick={closeMenu}>
                                Home
                            </Link>
                        </div>
                        <div className="menu-1">
                            <Link to={'/about'} onClick={closeMenu}>
                                About
                            </Link>
                        </div>
                        <div className="menu-1">
                            <Link to={'/parent-login'} onClick={closeMenu}>
                                {account ? 'Parent Portal' : 'Parent Login'}
                            </Link>
                        </div>
                        <div className="menu-1">
                            <Link to={'/agent-login'} onClick={closeMenu}>
                                {account ? 'Agent Portal' : 'Agent Login'}
                            </Link>
                        </div>
                        <div className="menu-1">
                            <Link to={'/about'} onClick={closeMenu}>
                                Contact
                            </Link>
                        </div>
                        {account && (
                            <div className="menu-1">
                                <button
                                    className="logoutButton"
                                    onClick={handleLogout}
                                >
                                    Logout
                                </button>
                            </div>
                        )}
                    </div>
                </div>
            )}
        </div>
    )
}

export default HamburgerMenu
