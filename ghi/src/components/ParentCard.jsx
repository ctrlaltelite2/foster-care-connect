import '../css/ChildCard.css'

const ParentCard = ({ parent }) => {
    return (
        <div className="childCard">
            <img
                src={parent.profile_picture}
                className="childCardPicture"
                alt={`${parent.parent1_name} and ${parent.parent2_name}`}
            />
            <div className="childCardInfo">
                <p>Parent: {parent.parent1_name}</p>
                <p>Parent: {parent.parent2_name}</p>
                <p>Location: {parent.location}</p>
            </div>
        </div>
    )
}

export default ParentCard
