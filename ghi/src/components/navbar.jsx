import HamburgerMenu from './HamburgerMenu'
import '../css/NavBar.css'
import fosterapp from '../assets/fosterapp.png'
import { Link } from 'react-router-dom'

const NavBar = () => {
    return (
        <nav className="NavBar">
            <div className="navLeft">
                <Link to={"/"}>
                    <img
                        className="fosterLogo"
                        src={fosterapp}
                        alt="FosterCare Connect Logo"
                    />
                </Link>
                <div>
                    <h1>FosterCare</h1>
                    <h1>Connect</h1>
                </div>
            </div>
            <div className="navRight">
                <HamburgerMenu />
            </div>
        </nav>
    )
}

export default NavBar
