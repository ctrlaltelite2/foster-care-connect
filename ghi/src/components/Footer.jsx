import '../css/Footer.css'
import twitter from '../assets/twitter.png'
import facebook from '../assets/facebook.png'
import insta from '../assets/insta.png'
import { Link } from 'react-router-dom'

const Footer = () => {
    return (
        <div className="Footer">
            <div className="footerTop">
                <div className="footerLeft">
                    <Link to={"/"}>Home</Link>
                    <Link to={"/about"}>About</Link>
                    <Link to={"/about"}>Contact Us</Link>
                </div>
                <div className="footerRight">
                    <Link to={"/"}>
                        <img
                            className="footerImg"
                            src={twitter}
                            alt="Twitter"
                        />
                    </Link>
                    <Link to={"/"}>
                        <img
                            className="footerImg"
                            src={facebook}
                            alt="Facebook"
                        />
                    </Link>
                    <Link to={"/"}>
                        <img
                            className="footerImg"
                            src={insta}
                            alt="Instagram"
                        />
                    </Link>
                </div>
            </div>
            <div className="footerBottom">
                <p>FosterCare Connect 2024</p>
            </div>
        </div>
    )
}

export default Footer
