import moment from 'moment'
import '../css/CustomCalendar.css'

const CustomCalendar = ({ appointments, childId }) => {
    const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    const currentDate = moment()
    const firstDayOfMonth = moment(currentDate).startOf('month')
    const daysInMonth = currentDate.daysInMonth()
    const startingDayOfWeek = firstDayOfMonth.day()
    const daysArray = Array.from({ length: daysInMonth }, (_, i) =>
        moment(firstDayOfMonth).add(i, 'days')
    )

    const filteredAppointments = appointments.filter(
        (appt) =>
            appt.children_id === parseInt(childId) &&
            moment(appt.date).isSame(currentDate, 'month')
    )


    const weeksArray = []
    let currentWeek = []

    for (let i = 0; i < startingDayOfWeek; i++) {
        currentWeek.push(null)
    }

    daysArray.forEach((day, index) => {
        currentWeek.push(day)
        if (currentWeek.length === 7 || index === daysArray.length - 1) {
            weeksArray.push([...currentWeek])
            currentWeek = []
        }
    })

    while (currentWeek.length < 7) {
        currentWeek.push(null)
    }

    return (
        <div className="CalenderPage">
            <table>
                <thead className="calendar-headers">
                    <tr>
                        <th className="calendar-header" colSpan="7">
                            {firstDayOfMonth.format('MMMM YYYY')}
                        </th>
                    </tr>
                    <tr>
                        {daysOfWeek.map((day) => (
                            <th key={day}>{day}</th>
                        ))}
                    </tr>
                </thead>
                <tbody className="calendar-content">
                    {weeksArray.map((week, index) => (
                        <tr key={index}>
                            {week.map((day, index) => (
                                <td className="calendar-Square" key={index}>
                                    <div className="calendar-Square-date">
                                        {day ? day.format('D') : ''}
                                    </div>
                                    {day &&
                                        filteredAppointments.map(
                                            (appointment) => {
                                                const appointmentDate = moment(
                                                    appointment.date
                                                )
                                                if (
                                                    day.isSame(
                                                        appointmentDate,
                                                        'day'
                                                    )
                                                ) {
                                                    return (
                                                        <div
                                                            className="calendar-Square-info"
                                                            key={appointment.id}
                                                        >
                                                            {appointment.reason}
                                                        </div>
                                                    )
                                                }
                                                return null
                                            }
                                        )}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default CustomCalendar
