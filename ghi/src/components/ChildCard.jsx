import '../css/ChildCard.css'

const ChildCard = ({ child, agents }) => {
    const agentMatch = agents
        ? agents.find((agent) => agent.id === child.agents_id)
        : null

    return (
        <div className="childCard">
            <img
                src={child.profile_picture}
                className="childCardPicture"
                alt={child.name}
            />
            <div className="childCardInfo">
                <p>{child.name}</p>
                <p>Age: {child.age}</p>
                <p>Agent: {agentMatch ? agentMatch.name : 'N/A'}</p>
            </div>
        </div>
    )
}

export default ChildCard
