import '../css/ChildCard.css'

const AgentChildCard = ({ child, parents }) => {
    const parentMatch = parents
        ? parents.find((parent) => parent.id === child.parents_id)
        : null

    return (
        <div className="childCard">
            <img
                src={child.profile_picture}
                className="childCardPicture"
                alt={child.name}
            />
            <div className="childCardInfo">
                <p>{child.name}</p>
                <p>Age: {child.age}</p>
                <p>
                    Parents: {parentMatch ? parentMatch.parent1_name : 'N/A'}{' '}
                    and {parentMatch ? parentMatch.parent2_name : 'N/A'}
                </p>
            </div>
        </div>
    )
}

export default AgentChildCard
