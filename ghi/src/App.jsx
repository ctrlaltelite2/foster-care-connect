import { Outlet } from 'react-router-dom'
import NavBar from './components/navbar'
import Footer from './components/Footer'
import './App.css'

const App = () => (
    <div className="site-container">
        <NavBar />
        <div className="content">
            <Outlet />
        </div>
        <Footer />
    </div>
)

export default App
