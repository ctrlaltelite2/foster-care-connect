# Foster Care Connect

- Kaylee Harding
- Aaron Straight
- Alexander Wilson
- Adrianna Jones

Foster Care Connect - Simplyfying the Paperwork

Foster Care Connect - We Handle Paperwork, so You can be the Parent They Need

## Design

- [API Design](./apis.md)
- [Data Models](./data-models.md)
- [GHI](./ghi.md)

## Intended Market

Our app is designed to provide convenience and organization to both Child Services Agents and Foster Parents. We present a centralized hub for all the mandatory reporting neccessary for every child in the foster care system. Now, parents and agents can easily access and provide documentation without the need for physical paperwork.

## Functionality

- Agents are the only ones with the ability to add parents and children to the system, offering security by limiting access to sensitive information to only the parents screened and managed by the agent
- An agent's profile is a dashboard to quickly access all the children in their care
    - Children can be filtered by parents
- Parents have a profile that lists pertinent information and easy access to the children's profiles they are fostering
- Child profiles easily show all documentation of Medical Appointments, Incident Reports, Medications, and Toys
    - Each list has buttons to add, edit, or delete the information for each type of documentation
- Our about page lists all of our contact information including email, gitlab link, and a link to a profile page if applicable

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone repository down to your local machine
2. CD into the new project directory
3. Create a .env file in the top level directory and fill it in according to [sample env](./sample-env.md)
4. Run ```docker volume create postgres-data```
5. Run ```docker compose build```
6. Run ```docker compose up```
7. In a webpage go to localhost:5173 and enjoy!
