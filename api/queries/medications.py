from pydantic import BaseModel
from queries.pool import pool
from datetime import date
from typing import List, Union, Optional


class Error(BaseModel):
    message: str


class MedicationIn(BaseModel):
    name: str
    dosage: str
    quantity: int
    expiration_date: date
    children_id: int


class MedicationOut(BaseModel):
    id: int
    name: str
    dosage: str
    quantity: int
    expiration_date: date
    children_id: int


class MedicationRepository:
    def create(self, medication: MedicationIn) -> Union[MedicationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO medications
                        (name,
                        dosage,
                        quantity,
                        expiration_date,
                        children_id)
                        VALUES
                        (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            medication.name,
                            medication.dosage,
                            medication.quantity,
                            medication.expiration_date,
                            medication.children_id,
                        ],
                    )
                    conn.commit()
                    id = result.fetchone()[0]
                    return MedicationOut(id=id, **medication.dict())
        except Exception as e:
            print(e)
            return Error(message="Create medication did not work")

    def update(
        self, medication_id: int, medication: MedicationIn
    ) -> Union[MedicationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE medications
                        SET
                        name = %s,
                        dosage = %s,
                        quantity = %s,
                        expiration_date = %s,
                        children_id = %s
                        WHERE id = %s;
                        """,
                        [
                            medication.name,
                            medication.dosage,
                            medication.quantity,
                            medication.expiration_date,
                            medication.children_id,
                            medication_id,
                        ],
                    )
                    return self.medication_in_to_out(medication_id, medication)
        except Exception as e:
            print(e)
            return Error(message="Could not update medication")

    def get_one(
        self, medication_id: int
    ) -> Union[Optional[MedicationOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        "SELECT * FROM medications WHERE id = %s;",
                        [medication_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_medication_out(record)
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve that medication")

    def delete(self, medication_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM medications
                        WHERE id = %s;
                        """,
                        [medication_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self) -> List[MedicationOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                   SELECT * FROM medications;
                   """
                )
                return [
                    self.record_to_medication_out(record) for record in result
                ]

    def medication_in_to_out(
        self, id: int, medication: MedicationIn
    ) -> MedicationOut:
        old_data = medication.dict()
        return MedicationOut(id=id, **old_data)

    def record_to_medication_out(self, record) -> MedicationOut:
        return MedicationOut(
            id=record[0],
            name=record[1],
            dosage=record[2],
            quantity=record[3],
            expiration_date=record[4],
            children_id=record[5],
        )
