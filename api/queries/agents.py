from pydantic import BaseModel
from queries.pool import pool
from typing import Union, List, Optional


class AgentIn(BaseModel):
    name: str
    employee_id: int
    account_id: int
    profile_picture: str


class AgentOut(BaseModel):
    id: int
    name: str
    employee_id: int
    account_id: int
    profile_picture: str


class Error(BaseModel):
    message: str


class AgentsRepository:
    def create(self, agent: AgentIn) -> Union[AgentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO agents
                        (name,
                        employee_id,
                        account_id,
                        profile_picture)
                        VALUES (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            agent.name,
                            agent.employee_id,
                            agent.account_id,
                            agent.profile_picture,
                        ],
                    )
                    conn.commit()
                    id = result.fetchone()[0]
                    return self.agent_in_to_out(id, agent)
        except Exception as e:
            print(e)
            return Error(message="Create agent did not work")

    def get_all(self) -> Union[List[AgentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM agents ORDER BY id;
                        """
                    )
                    result = db.fetchall()
                    return [
                        self.record_to_agent_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve agents")

    def update(self, agent_id: int, agent: AgentIn) -> Union[AgentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE agents
                        SET name = %s,
                        employee_id = %s,
                        account_id = %s,
                        profile_picture = %s
                        WHERE id = %s;
                        """,
                        [
                            agent.name,
                            agent.employee_id,
                            agent.account_id,
                            agent.profile_picture,
                            agent_id,
                        ],
                    )
                    conn.commit()
                    return self.agent_in_to_out(agent_id, agent)
        except Exception as e:
            print(e)
            return Error(message="Could not update agent")

    def delete(self, agent_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM agents WHERE id = %s;
                        """,
                        [agent_id],
                    )
                    conn.commit()
                    return True
        except Exception as e:
            print(e)
            return Error(message="Could not delete agent")

    def get_one(self, agent_id: int) -> Union[Optional[AgentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM agents WHERE id = %s;
                        """,
                        [agent_id],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return self.record_to_agent_out(record)
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve that agent")

    def agent_in_to_out(self, id: int, agent: AgentIn) -> AgentOut:
        old_data = agent.dict()
        return AgentOut(id=id, **old_data)

    def record_to_agent_out(self, record) -> AgentOut:
        return AgentOut(
            id=record[0],
            name=record[2],
            employee_id=record[3],
            account_id=record[4],
            profile_picture=record[1],
        )
