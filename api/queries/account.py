from pydantic import BaseModel
from queries.pool import pool


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str


class AccountOut(BaseModel):
    id: int
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepository:
    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO accounts (username, hashed_password)
                    VALUES (%s, %s)
                    RETURNING id;
                    """,
                    [account.username, hashed_password],
                )
                id = result.fetchone()[0]
                return self.account_in_to_out(id, account, hashed_password)

    def account_in_to_out(
        self, id: int, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        return AccountOutWithPassword(
            id=id, username=account.username, hashed_password=hashed_password
        )

    def get_one(self, username: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, username, hashed_password
                        FROM accounts
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = db.fetchone()
                    if record:
                        return AccountOutWithPassword(
                            id=record[0],
                            username=record[1],
                            hashed_password=record[2],
                        )
                    else:
                        return None
        except Exception as e:
            print(e)
            return None

    def update(
        self, id: int, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE accounts
                        SET username = %s, hashed_password = %s
                        WHERE id = %s
                        RETURNING id, username, hashed_password;
                        """,
                        [account.username, hashed_password, id],
                    )
                    record = db.fetchone()
                    if record:
                        return AccountOutWithPassword(
                            id=record[0],
                            username=record[1],
                            hashed_password=record[2],
                        )
                    else:
                        raise ValueError("Account not found or update failed")
        except Exception as e:
            print(f"Error updating account: {e}")
            raise

    def delete(self, id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM accounts
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    if db.rowcount == 0:
                        raise ValueError("Account not found or delete failed")
        except Exception as e:
            print(f"Error deleting account: {e}")
            raise

    def get_by_id(self, id: int) -> AccountOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, username
                        FROM accounts
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    record = db.fetchone()
                    if record:
                        return AccountOut(id=record[0], username=record[1])
                    else:
                        return None
        except Exception as e:
            print(f"Error fetching account by ID: {e}")
            return None
