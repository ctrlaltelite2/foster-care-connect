from pydantic import BaseModel
from queries.pool import pool
from typing import Union, List, Optional


class ParentIn(BaseModel):
    parent1_name: str
    parent2_name: str
    location: str
    account_id: int
    profile_picture: str


class ParentOut(BaseModel):
    id: int
    parent1_name: str
    parent2_name: str
    location: str
    account_id: int
    profile_picture: str


class Error(BaseModel):
    message: str


class ParentsRepository:
    def create(self, parent: ParentIn) -> Union[ParentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO parents
                        (parent1_name,
                        parent2_name,
                        location,
                        account_id,
                        profile_picture)
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            parent.parent1_name,
                            parent.parent2_name,
                            parent.location,
                            parent.account_id,
                            parent.profile_picture,
                        ],
                    )
                    conn.commit()
                    id = result.fetchone()[0]
                    return self.parent_in_to_out(id, parent)
        except Exception as e:
            print(e)
            return Error(message="Create parent did not work")

    def get_all(self) -> Union[List[ParentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM parents ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_parent_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve parents")

    def update(
        self, parent_id: int, parent: ParentIn
    ) -> Union[ParentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE parents
                        SET
                        parent1_name = %s,
                        parent2_name = %s,
                        location = %s,
                        account_id = %s,
                        profile_picture = %s
                        WHERE id = %s;
                        """,
                        [
                            parent.parent1_name,
                            parent.parent2_name,
                            parent.location,
                            parent.account_id,
                            parent.profile_picture,
                            parent_id,
                        ],
                    )
                    return self.parent_in_to_out(parent_id, parent)
        except Exception as e:
            print(e)
            return Error(message="Could not update parent")

    def delete(self, parent_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM parents WHERE id = %s;
                        """,
                        [parent_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, parent_id: int) -> Union[Optional[ParentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM parents WHERE id = %s;
                        """,
                        [parent_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_parent_out(record)
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve that parent")

    def parent_in_to_out(self, id: int, parent: ParentIn) -> ParentOut:
        old_data = parent.dict()
        return ParentOut(id=id, **old_data)

    def record_to_parent_out(self, record) -> ParentOut:
        return ParentOut(
            id=record[0],
            parent1_name=record[2],
            parent2_name=record[3],
            location=record[4],
            account_id=record[5],
            profile_picture=record[1],
        )
