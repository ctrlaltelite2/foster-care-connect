from pydantic import BaseModel
from queries.pool import pool
from typing import List, Union
from datetime import date


class Error(BaseModel):
    message: str


class Med_apptIn(BaseModel):
    date: date
    location: str
    reason: str
    description_of_care: str
    treatment_plan: str
    children_id: int


class Med_apptOut(BaseModel):
    id: int
    date: date
    location: str
    reason: str
    description_of_care: str
    treatment_plan: str
    children_id: int


class Med_apptNotFoundError(Exception):
    """
    Exception raised when an med_appt record is not found.
    """

    pass


class Med_apptsRepository:
    def create(self, med_appt: Med_apptIn) -> Med_apptOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO med_appts (
                        date
                    , location
                    , reason
                    , description_of_care
                    , treatment_plan
                    , children_id
                    )
                    VALUES (%s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        med_appt.date,
                        med_appt.location,
                        med_appt.reason,
                        med_appt.description_of_care,
                        med_appt.treatment_plan,
                        med_appt.children_id,
                    ],
                )
                conn.commit()
                id = result.fetchone()[0]
                return Med_apptOut(id=id, **med_appt.dict())

    def get_one(self, med_appt_id: int) -> Med_apptOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , date
                        , location
                        , reason
                        , description_of_care
                        , treatment_plan
                        , children_id
                        FROM med_appts
                        WHERE id = %s
                        """,
                        [med_appt_id],
                    )
                    record = result.fetchone()
                    if record:
                        return Med_apptOut(
                            id=record[0],
                            date=record[1],
                            location=record[2],
                            reason=record[3],
                            description_of_care=record[4],
                            treatment_plan=record[5],
                            children_id=record[6],
                        )
                    else:
                        raise Med_apptNotFoundError(
                            f"Med_appt with id {med_appt_id} not found"
                        )
        except Med_apptNotFoundError as e:
            raise e
        except Exception as e:
            print(f"An error occurred while retrieving med_appt: {e}")
            raise Exception("Failed to retrieve med_appt information")

    def update(
        self, med_appt_id: int, med_appt: Med_apptIn
    ) -> Union[Med_apptOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE med_appts
                        SET date = %s
                        , location = %s
                        , reason = %s
                        , description_of_care = %s
                        , treatment_plan = %s
                        , children_id = %s
                        WHERE id = %s;
                        """,
                        (
                            med_appt.date,
                            med_appt.location,
                            med_appt.reason,
                            med_appt.description_of_care,
                            med_appt.treatment_plan,
                            med_appt.children_id,
                            med_appt_id,
                        ),
                    )
                    return self.med_appt_in_to_out(med_appt_id, med_appt)
        except Exception as e:
            print(e)
            return Error(message="Could not update medical appointment")

    def delete(self, med_appt_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM med_appts
                    WHERE id = %s;
                    """,
                    (med_appt_id,),
                )
                conn.commit()
                return db.rowcount > 0

    def get_all(self) -> List[Med_apptOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM med_appts;
                    """
                )
                return [
                    self.record_to_med_appt_out(record) for record in result
                ]

    def med_appt_in_to_out(self, id: int, med_appt: Med_apptIn) -> Med_apptOut:
        old_data = med_appt.dict()
        return Med_apptOut(id=id, **old_data)

    def record_to_med_appt_out(self, record) -> Med_apptOut:
        return Med_apptOut(
            id=record[0],
            date=record[1],
            location=record[2],
            reason=record[3],
            description_of_care=record[4],
            treatment_plan=record[5],
            children_id=record[6],
        )
