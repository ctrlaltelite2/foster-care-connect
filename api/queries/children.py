from pydantic import BaseModel
from queries.pool import pool
from typing import List


class Error(BaseModel):
    message: str


class ChildIn(BaseModel):
    profile_picture: str
    name: str
    age: int
    parents_id: int
    agents_id: int


class ChildOut(BaseModel):
    id: int
    profile_picture: str
    name: str
    age: int
    parents_id: int
    agents_id: int


class ChildrenRepository:
    def create(self, child: ChildIn) -> ChildOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO children
                    (profile_picture, name, age, parents_id, agents_id)
                    VALUES
                    (%s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        child.profile_picture,
                        child.name,
                        child.age,
                        child.parents_id,
                        child.agents_id,
                    ],
                )
                conn.commit()
                id = result.fetchone()[0]
                return ChildOut(id=id, **child.dict())

    def get_one(self, child_id: int) -> ChildOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id,
                    profile_picture,
                    name, age,
                    parents_id,
                    agents_id
                    FROM children
                    WHERE id = %s;
                    """,
                    [
                        child_id,
                    ],
                )
                record = result.fetchone()
                if record:
                    return ChildOut(
                        id=record[0],
                        profile_picture=record[1],
                        name=record[2],
                        age=record[3],
                        parents_id=record[4],
                        agents_id=record[5],
                    )
                else:
                    return Error(message="Child not found")

    def update(self, child_id: int, child: ChildIn) -> ChildOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE children
                    SET profile_picture = %s,
                    name = %s,
                    age = %s,
                    parents_id = %s,
                    agents_id = %s
                    WHERE id = %s;
                    """,
                    [
                        child.profile_picture,
                        child.name,
                        child.age,
                        child.parents_id,
                        child.agents_id,
                        child_id,
                    ],
                )
                conn.commit()
                return self.children_in_to_out(child_id, child)

    def delete(self, child_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM children
                    WHERE id = %s;
                    """,
                    [
                        child_id,
                    ],
                )
                conn.commit()
                return db.rowcount > 0

    def get_all(self) -> List[ChildOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM children;
                    """
                )
                return [
                    self.record_to_children_out(record) for record in result
                ]

    def children_in_to_out(self, id: int, child: ChildIn):
        old_data = child.dict()
        return ChildOut(id=id, **old_data)

    def record_to_children_out(self, record):
        return ChildOut(
            id=record[0],
            profile_picture=record[1],
            name=record[2],
            age=record[3],
            parents_id=record[4],
            agents_id=record[5],
        )
