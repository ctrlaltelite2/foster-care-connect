from pydantic import BaseModel
from queries.pool import pool
from typing import Union, List, Optional


class Error(BaseModel):
    message: str


class IncidentsIn(BaseModel):
    name: str
    picture: str
    description: str
    police_involved: bool
    action_plan: str
    children_id: int


class IncidentsOut(BaseModel):
    id: int
    name: str
    picture: str
    description: str
    police_involved: bool
    action_plan: str
    children_id: int


class IncidentsRepository:
    def create(self, incidents: IncidentsIn) -> Union[IncidentsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO incidents
                            (name,
                            picture,
                            description,
                            police_involved,
                            action_plan,
                            children_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            incidents.name,
                            incidents.picture,
                            incidents.description,
                            incidents.police_involved,
                            incidents.action_plan,
                            incidents.children_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.incidents_in_to_out(id, incidents)
        except Exception as e:
            print(e)
            return {"message": "Create incident did not work"}

    def get_all(self) -> Union[List[IncidentsOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , name
                        , picture
                        , description
                        , police_involved
                        , action_plan
                        , children_id
                        FROM incidents
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_incidents_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve incidents"}

    def update(
        self, incident_id: int, incidents: IncidentsIn
    ) -> Union[IncidentsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE incidents
                        SET name = %s
                        , picture = %s
                        , description = %s
                        , police_involved = %s
                        , action_plan = %s
                        , children_id = %s
                        WHERE id = %s;
                        """,
                        [
                            incidents.name,
                            incidents.picture,
                            incidents.description,
                            incidents.police_involved,
                            incidents.action_plan,
                            incidents.children_id,
                            incident_id,
                        ],
                    )
                    return self.incidents_in_to_out(incident_id, incidents)
        except Exception as e:
            print(e)
            return {"message": "Could not update incident"}

    def delete(self, incident_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM incidents
                        WHERE id = %s;
                        """,
                        [incident_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, incident_id: int) -> Optional[IncidentsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , name
                        , picture
                        , description
                        , police_involved
                        , action_plan
                        , children_id
                        FROM incidents
                        WHERE id = %s;
                        """,
                        [incident_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_incidents_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve that incident"}

    def incidents_in_to_out(self, id: int, incidents: IncidentsIn):
        old_data = incidents.dict()
        return IncidentsOut(id=id, **old_data)

    def record_to_incidents_out(self, record):
        return IncidentsOut(
            id=record[0],
            name=record[1],
            picture=record[2],
            description=record[3],
            police_involved=record[4],
            action_plan=record[5],
            children_id=record[6],
        )
