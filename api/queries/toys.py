from pydantic import BaseModel
from queries.pool import pool
from typing import List


class Error(BaseModel):
    message: str


class ToyIn(BaseModel):
    name: str
    picture: str
    description: str
    condition: str
    children_id: int


class ToyOut(BaseModel):
    id: int
    name: str
    picture: str
    description: str
    condition: str
    children_id: int


class ToysRepository:
    def create(self, toy: ToyIn) -> ToyOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO toys
                    (name
                    , picture
                    , description
                    , condition
                    , children_id)
                    VALUES
                    (%s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        toy.name,
                        toy.picture,
                        toy.description,
                        toy.condition,
                        toy.children_id,
                    ],
                )
                conn.commit()
                id = result.fetchone()[0]
                return self.toys_in_to_out(id, toy)

    def get_one(self, toy_id: int) -> ToyOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , name
                        , picture
                        , description
                        , condition
                        , children_id
                        FROM toys
                        WHERE id = %s
                        """,
                        (toy_id,),
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_toys_out(record)
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve toy information")

    def update(self, toy_id: int, toy: ToyIn) -> ToyOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE toys
                    SET name = %s
                    , picture = %s
                    , description = %s
                    , condition = %s
                    , children_id = %s
                    WHERE id = %s
                    RETURNING *;
                    """,
                    (
                        toy.name,
                        toy.picture,
                        toy.description,
                        toy.condition,
                        toy.children_id,
                        toy_id,
                    ),
                )
                record = db.fetchone()
                conn.commit()
                if record:
                    return self.toys_in_to_out(toy_id, toy)
                else:
                    raise Exception("Toy not found or update failed")

    def delete(self, toy_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM toys
                        WHERE id = %s;
                        """,
                        (toy_id,),
                    )
                    conn.commit()
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self) -> List[ToyOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM toys;
                    """
                )
                return [self.record_to_toys_out(record) for record in result]

    def toys_in_to_out(self, id: int, toy: ToyIn):
        old_data = toy.dict()
        return ToyOut(id=id, **old_data)

    def record_to_toys_out(self, record):
        return ToyOut(
            id=record[0],
            name=record[1],
            picture=record[2],
            description=record[3],
            condition=record[4],
            children_id=record[5],
        )
