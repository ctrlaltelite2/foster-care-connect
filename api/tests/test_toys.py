from fastapi.testclient import TestClient
from main import app
from queries.toys import ToyOut, ToysRepository

import os

os.environ["DATABASE_URL"] = os.environ.get("DATABASE_URL")


class FakeToysRepository:
    def get_all(self):
        return [
            ToyOut(
                id=1,
                name="Toy 1",
                picture="toy1_picture",
                description="Toy 1 description",
                condition="New",
                children_id=1,
            ),
            ToyOut(
                id=2,
                name="Toy 2",
                picture="toy2_picture",
                description="Toy 2 description",
                condition="Used",
                children_id=2,
            ),
        ]


def test_get_toys():
    """
    Test get toys endpoint.
    """

    app.dependency_overrides[ToysRepository] = FakeToysRepository

    client = TestClient(app)
    response = client.get("/api/toys")

    assert response.status_code == 200
    toys = response.json()
    assert len(toys) == 2
    assert toys[0]["name"] == "Toy 1"
    assert toys[1]["name"] == "Toy 2"

    app.dependency_overrides = {}
