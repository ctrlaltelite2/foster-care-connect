from fastapi.testclient import TestClient
from main import app
from queries.medications import MedicationOut, MedicationRepository
from datetime import date
import os

os.environ["DATABASE_URL"] = os.environ.get("DATABASE_URL")

client = TestClient(app)


class MockMedicationQueries:
    """
    Mock version of medication repo
    """

    def get_one(self, id: int):
        return MedicationOut(
            id=id,
            name="Medication 1",
            dosage="Dosage 1",
            quantity=3,
            expiration_date=date(2024, 1, 30),
            children_id=1,
        )


def test_get_one_medication():
    """
    Test to get one med_appt endpoint
    """
    app.dependency_overrides[MedicationRepository] = MockMedicationQueries
    id = 1

    response = client.get(f"/api/medications/{id}")

    assert response.status_code == 200

    app.dependency_overrides = {}
