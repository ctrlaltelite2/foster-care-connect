from fastapi.testclient import TestClient
from queries.incidents import (
    IncidentsOut,
    IncidentsRepository,
)
from main import app
import os

os.environ["DATABASE_URL"] = os.environ.get("DATABASE_URL")

client = TestClient(app)


class FakeIncidentsRepository:
    def get_all(self):
        return [
            IncidentsOut(
                id=1,
                name="Incident 1",
                picture="placeholder_picture",
                description="incident 1 description",
                police_involved=True,
                action_plan="incident 1 action plan",
                children_id=1,
            ),
            IncidentsOut(
                id=2,
                name="Incident 2",
                picture="placeholder_picture",
                description="incident 2 description",
                police_involved=True,
                action_plan="incident 2 action plan",
                children_id=2,
            ),
        ]


def test_get_incidents():
    """
    test get incidents endpoint
    """

    app.dependency_overrides[IncidentsRepository] = FakeIncidentsRepository

    response = client.get("/api/incidents")
    assert response.status_code == 200
    incidents = response.json()
    assert len(incidents) == 2

    app.dependency_overrides = {}
