from fastapi.testclient import TestClient
from main import app
from queries.med_appts import Med_apptOut, Med_apptsRepository
from datetime import date
import os

os.environ["DATABASE_URL"] = os.environ.get("DATABASE_URL")
client = TestClient(app)


class MockMedApptQueries:
    """
    Mock version of med appts repo
    """

    def get_one(self, id: int):
        return Med_apptOut(
            id=id,
            date=date(2024, 1, 30),
            location="test",
            reason="test",
            description_of_care="test",
            treatment_plan="test",
            children_id=1,
        )


def test_get_one_medappt():
    """
    Test to get one med_appt endpoint
    """
    app.dependency_overrides[Med_apptsRepository] = MockMedApptQueries
    id = 1

    response = client.get(f"/api/med_appts/{id}")

    assert response.status_code == 200

    app.dependency_overrides = {}
