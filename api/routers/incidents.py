from fastapi import APIRouter, Depends, HTTPException, status
from typing import List, Optional, Union
from queries.incidents import (
    Error,
    IncidentsRepository,
    IncidentsIn,
    IncidentsOut,
)

router = APIRouter()


@router.post("/api/incidents", response_model=Union[IncidentsOut, Error])
def create_incident(
    incidents: IncidentsIn,
    repo: IncidentsRepository = Depends(),
):
    try:
        return repo.create(incidents)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, message=str(e)
        )


@router.get("/api/incidents", response_model=Union[List[IncidentsOut], Error])
def get_all_incidents(repo: IncidentsRepository = Depends()):
    try:
        return repo.get_all()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, message=str(e)
        )


@router.put(
    "/api/incidents/{incident_id}", response_model=Union[IncidentsOut, Error]
)
def update_incident(
    incident_id: int,
    incidents: IncidentsIn,
    repo: IncidentsRepository = Depends(),
) -> Union[IncidentsOut, Error]:
    try:
        return repo.update(incident_id, incidents)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, message=str(e)
        )


@router.delete("/api/incidents/{incident_id}", response_model=bool)
def delete_incident(
    incident_id: int,
    repo: IncidentsRepository = Depends(),
) -> bool:
    try:
        return repo.delete(incident_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, message=str(e)
        )


@router.get(
    "/api/incidents/{incident_id}", response_model=Optional[IncidentsOut]
)
def get_one_incident(
    incident_id: int,
    repo: IncidentsRepository = Depends(),
) -> IncidentsOut:
    try:
        return repo.get_one(incident_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, message=str(e)
        )
