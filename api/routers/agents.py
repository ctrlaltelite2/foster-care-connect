from fastapi import Depends, HTTPException, status, Response, APIRouter
from pydantic import BaseModel
from typing import List

from queries.agents import AgentIn, AgentOut, AgentsRepository


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/agents", response_model=AgentOut | HttpError)
async def create_agent(
    agent_info: AgentIn,
    repo: AgentsRepository = Depends(),
):
    try:
        agent = repo.create(agent_info)
        return agent
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(exc)
        )


@router.get("/api/agents/{agent_id}", response_model=AgentOut | HttpError)
async def get_agent(
    agent_id: int,
    repo: AgentsRepository = Depends(),
):
    try:
        agent = repo.get_one(agent_id)
        if agent:
            return agent
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Agent not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.put("/api/agents/{agent_id}", response_model=AgentOut | HttpError)
async def update_agent(
    agent_id: int,
    agent_info: AgentIn,
    repo: AgentsRepository = Depends(),
):
    try:
        updated_agent = repo.update(agent_id, agent_info)
        if updated_agent:
            return updated_agent
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Agent not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.delete("/api/agents/{agent_id}", response_model=HttpError | None)
async def delete_agent(
    agent_id: int,
    repo: AgentsRepository = Depends(),
):
    try:
        success = repo.delete(agent_id)
        if success:
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Agent not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.get("/api/agents", response_model=List[AgentOut])
async def get_all_agents(repo: AgentsRepository = Depends()):
    try:
        agents = repo.get_all()
        return agents
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )
