from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
)
from pydantic import BaseModel
from typing import List


from queries.medications import (
    MedicationIn,
    MedicationOut,
    MedicationRepository,
)


class MedicationForm(BaseModel):
    name: str
    age: int
    quantity: str
    expiration_date: int
    children_id: int


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/medications", response_model=MedicationOut | HttpError)
async def create_medication(
    medication_info: MedicationIn,
    repo: MedicationRepository = Depends(),
):
    try:
        medication = repo.create(medication_info)
        return medication
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(exc)
        )


@router.get(
    "/api/medications/{medication_id}",
    response_model=MedicationOut | HttpError,
)
async def get_medication(
    medication_id: int,
    repo: MedicationRepository = Depends(),
):
    try:
        medication = repo.get_one(medication_id)
        if medication:
            return medication
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Medication not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.put(
    "/api/medications/{medication_id}",
    response_model=MedicationOut | HttpError,
)
async def update_medication(
    medication_id: int,
    medication_info: MedicationIn,
    repo: MedicationRepository = Depends(),
):
    try:
        updated_medication = repo.update(medication_id, medication_info)
        if updated_medication:
            return updated_medication
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Medication not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.delete(
    "/api/medications/{medication_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": HttpError}},
)
async def delete_medication(
    medication_id: int,
    repo: MedicationRepository = Depends(),
):
    try:
        deleted = repo.delete(medication_id)
        if deleted:
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Medication not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.get("/api/medications", response_model=List[MedicationOut])
async def get_all_medications(repo: MedicationRepository = Depends()):
    try:
        medications = repo.get_all()
        return medications
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )
