from fastapi import Depends, HTTPException, status, Response, APIRouter
from pydantic import BaseModel
from typing import List

from queries.med_appts import Med_apptIn, Med_apptOut, Med_apptsRepository


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/med_appts", response_model=Med_apptOut | HttpError)
async def create_med_appt(
    med_appt_info: Med_apptIn,
    repo: Med_apptsRepository = Depends(),
):
    try:
        med_appt = repo.create(med_appt_info)
        return med_appt
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(exc)
        )


@router.get(
    "/api/med_appts/{med_appt_id}", response_model=Med_apptOut | HttpError
)
async def get_med_appt(
    med_appt_id: int,
    repo: Med_apptsRepository = Depends(),
):
    try:
        med_appt = repo.get_one(med_appt_id)
        if med_appt:
            return med_appt
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Med_appt not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.put(
    "/api/med_appts/{med_appt_id}", response_model=Med_apptOut | HttpError
)
async def update_med_appt(
    med_appt_id: int,
    med_appt_info: Med_apptIn,
    repo: Med_apptsRepository = Depends(),
):
    try:
        updated_med_appt = repo.update(med_appt_id, med_appt_info)
        if updated_med_appt:
            return updated_med_appt
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Med_appt not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.delete("/api/med_appts/{med_appt_id}", response_model=HttpError | None)
async def delete_med_appt(
    med_appt_id: int,
    repo: Med_apptsRepository = Depends(),
):
    try:
        success = repo.delete(med_appt_id)
        if success:
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Med_appt not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.get("/api/med_appts", response_model=List[Med_apptOut])
async def get_all_med_appts(repo: Med_apptsRepository = Depends()):
    try:
        med_appts = repo.get_all()
        return med_appts
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )
