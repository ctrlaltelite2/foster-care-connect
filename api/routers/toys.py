from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
)
from pydantic import BaseModel
from typing import List

from queries.toys import (
    ToyIn,
    ToyOut,
    ToysRepository,
)

from jwtdown_fastapi.authentication import Token


class ToyForm(BaseModel):
    name: str
    picture: str
    description: str
    condition: str
    children_id: int


class ToyToken(Token):
    toy: ToyOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/toys", response_model=ToyOut | HttpError)
async def create_toy(
    toy_info: ToyIn,
    response: Response,
    repo: ToysRepository = Depends(),
):
    try:
        toy = repo.create(toy_info)
        return toy
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(exc)
        )


@router.get("/api/toys/{toy_id}", response_model=ToyOut | HttpError)
async def get_toy(
    toy_id: int,
    repo: ToysRepository = Depends(),
):
    try:
        toy = repo.get_one(toy_id)
        if toy:
            return toy
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Toy not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.put("/api/toys/{toy_id}", response_model=ToyOut | HttpError)
async def update_toy(
    toy_id: int,
    toy_info: ToyIn,
    repo: ToysRepository = Depends(),
):
    try:
        updated_toy = repo.update(toy_id, toy_info)
        if updated_toy:
            return updated_toy
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Toy not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.delete("/api/toys/{toy_id}", response_model=HttpError | None)
async def delete_toy(
    toy_id: int,
    repo: ToysRepository = Depends(),
):
    try:
        deleted = repo.delete(toy_id)
        if deleted:
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Toy not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.get("/api/toys", response_model=List[ToyOut])
async def get_all_Toys(repo: ToysRepository = Depends()):
    try:
        toys = repo.get_all()
        return toys
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )
