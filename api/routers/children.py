from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
)
from pydantic import BaseModel
from typing import List

from queries.children import (
    ChildIn,
    ChildOut,
    ChildrenRepository,
)

from jwtdown_fastapi.authentication import Token


class ChildForm(BaseModel):
    name: str
    age: int
    profile_picture: str
    parents_id: int
    agents_id: int


class ChildToken(Token):
    child: ChildOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/children", response_model=ChildOut | HttpError)
async def create_child(
    child_info: ChildIn,
    response: Response,
    repo: ChildrenRepository = Depends(),
):
    try:
        child = repo.create(child_info)
        return child
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(exc)
        )


@router.get("/api/children/{child_id}", response_model=ChildOut | HttpError)
async def get_child(
    child_id: int,
    repo: ChildrenRepository = Depends(),
):
    try:
        child = repo.get_one(child_id)
        if child:
            return child
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Child not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.put("/api/children/{child_id}", response_model=ChildOut | HttpError)
async def update_child(
    child_id: int,
    child_info: ChildIn,
    repo: ChildrenRepository = Depends(),
):
    try:
        updated_child = repo.update(child_id, child_info)
        if updated_child:
            return updated_child
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Child not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.delete("/api/children/{child_id}", response_model=HttpError | None)
async def delete_child(
    child_id: int,
    repo: ChildrenRepository = Depends(),
):
    try:
        deleted = repo.delete(child_id)
        if deleted:
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Child not found"
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.get("/api/children", response_model=List[ChildOut])
async def get_all_children(repo: ChildrenRepository = Depends()):
    try:
        children = repo.get_all()
        return children
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )
