from fastapi import Depends, HTTPException, status, APIRouter, Response
from pydantic import BaseModel
from typing import List

from queries.parents import ParentIn, ParentOut, ParentsRepository


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post(
    "/api/parents",
    response_model=ParentOut | HttpError,
    status_code=status.HTTP_201_CREATED,
)
async def create_parent(
    parent_info: ParentIn,
    repo: ParentsRepository = Depends(),
):
    try:
        parent = repo.create(parent_info)
        return parent
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(exc)
        )


@router.get("/api/parents/{parent_id}", response_model=ParentOut | HttpError)
async def get_parent(
    parent_id: int,
    repo: ParentsRepository = Depends(),
):
    try:
        parent = repo.get_one(parent_id)
        if parent:
            return parent
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Parent not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.put("/api/parents/{parent_id}", response_model=ParentOut | HttpError)
async def update_parent(
    parent_id: int,
    parent_info: ParentIn,
    repo: ParentsRepository = Depends(),
):
    try:
        updated_parent = repo.update(parent_id, parent_info)
        if updated_parent:
            return updated_parent
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Parent not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.delete(
    "/api/parents/{parent_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": HttpError}, 500: {"model": HttpError}},
)
async def delete_parent(
    parent_id: int,
    repo: ParentsRepository = Depends(),
):
    try:
        success = repo.delete(parent_id)
        if success:
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Parent not found",
            )
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )


@router.get("/api/parents", response_model=List[ParentOut])
async def get_all_parents(repo: ParentsRepository = Depends()):
    try:
        parents = repo.get_all()
        return parents
    except Exception as exc:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(exc)
        )
