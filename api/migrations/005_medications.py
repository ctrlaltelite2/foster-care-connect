steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS medications (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            dosage VARCHAR(255) NOT NULL,
            quantity INTEGER NOT NULL,
            expiration_date DATE NOT NULL,
            children_id INTEGER REFERENCES children (id)
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS medications;
        """,
    ]
]
