steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS accounts (
            id SERIAL PRIMARY KEY,
            username VARCHAR(255) UNIQUE NOT NULL,
            hashed_password VARCHAR(255) NOT NULL
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS accounts;
        """,
    ]
]
