steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS toys(
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            picture TEXT,
            description TEXT NOT NULL,
            condition TEXT NOT NULL,
            children_id INTEGER REFERENCES children (id)
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS toys;
        """,
    ]
]
