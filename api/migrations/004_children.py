steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS children (
            id SERIAL PRIMARY KEY,
            profile_picture TEXT,
            name VARCHAR(255) NOT NULL,
            age INTEGER NOT NULL,
            parents_id INTEGER REFERENCES parents (id),
            agents_id INTEGER REFERENCES agents (id)
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS children;
        """,
    ]
]
