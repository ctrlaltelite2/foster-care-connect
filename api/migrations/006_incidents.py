steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS incidents(
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            picture TEXT,
            description TEXT NOT NULL,
            police_involved BOOL NOT NULL,
            action_plan TEXT NOT NULL,
            children_id INTEGER REFERENCES children (id)
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS incidents;
        """,
    ]
]
