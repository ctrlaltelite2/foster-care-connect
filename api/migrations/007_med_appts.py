steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS med_appts(
            id SERIAL PRIMARY KEY,
            date DATE NOT NULL,
            location VARCHAR(255) NOT NULL,
            reason TEXT NOT NULL,
            description_of_care TEXT NOT NULL,
            treatment_plan TEXT NOT NULL,
            children_id INTEGER REFERENCES children (id)
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS med_appts;
        """,
    ]
]
