steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS parents (
            id SERIAL PRIMARY KEY,
            profile_picture TEXT,
            parent1_name VARCHAR(255) NOT NULL,
            parent2_name VARCHAR(255) NOT NULL,
            location TEXT NOT NULL,
            account_id INTEGER REFERENCES accounts (id) UNIQUE NOT NULL
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS parents;
        """,
    ]
]
