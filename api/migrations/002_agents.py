steps = [
    [
        # Create the table
        """
        CREATE TABLE IF NOT EXISTS agents (
            id SERIAL PRIMARY KEY,
            profile_picture TEXT,
            name VARCHAR(255) NOT NULL,
            employee_id INTEGER NOT NULL,
            account_id INTEGER REFERENCES accounts (id) UNIQUE NOT NULL
        );
        """,
        # Drop the table
        """
        DROP TABLE IF EXISTS agents;
        """,
    ]
]
