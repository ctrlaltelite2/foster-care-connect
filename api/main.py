from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import (
    account,
    agents,
    children,
    incidents,
    toys,
    med_appts,
    medications,
    parents,
)
from authenticator import authenticator

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }


app.include_router(authenticator.router, tags=["authenticator"])
app.include_router(account.router, tags=["authenticator"])
app.include_router(agents.router, tags=["Agents"])
app.include_router(children.router, tags=["Children"])
app.include_router(incidents.router, tags=["Incidents"])
app.include_router(toys.router, tags=["Toys"])
app.include_router(medications.router, tags=["Medications"])
app.include_router(med_appts.router, tags=["Medical Appointments"])
app.include_router(parents.router, tags=["Parents"])
