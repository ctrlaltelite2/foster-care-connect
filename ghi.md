# Customer Graphical Human Interface

## Excalidraw of MVP

![alt text](./Readme-Images/excalidraw.png)

This is an outline of what we defined our minimum viable product (MVP). We wanted to have, at the minimum, a product that allows parents to track mandatory reports for their foster children and give the ability for Agents to track all the children assigned to them

## Home Page

![alt text](./Readme-Images/homePage.png)

The home page has the two major paths for our app, Parents and Agents, large and clear. The bottom of the page will give new users a glimpse at the funcunality of the app.
Mock up done using Figma

## Parent/Agent Sign In

![alt text](./Readme-Images/parentSignIn.png)
![alt text](./Readme-Images/agentSignIn.png)

Both sign in forms are clear with the information needed and set with error notifications in the event of bad login information. On a successful login, they redirect to the profile page of the user.
Both mock ups done with Figma

## Parent/Agent/Child's Profiles

![alt text](./Readme-Images/parentsProfile.png)
![alt text](./Readme-Images/agentsProfile.png)
![alt text](./Readme-Images/childProfile.png)

Our intention for the profile pages was to present all the neccesary information in an easy dashboard setting with cards that link to the children shown.
All mock ups done with Figma

## Forms

![alt text](./Readme-Images/incidentForm.png)
![alt text](./Readme-Images/medApptForm.png)
![alt text](./Readme-Images/medicationForm.png)
![alt text](./Readme-Images/toyForm.png)

The forms all have built in error notification and a notification of success if done to completion.
Form mock ups done with Figma

## Add Child/Parents Forms

![alt text](./Readme-Images/addChildForm.png)
![alt text](./Readme-Images/addParentsForm.png)

The add parent and add child forms are only avalible to the agents when they are signed in and on their profile.
Form mock ups done with Figma
